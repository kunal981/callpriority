package pro.com;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import pro.com.data.ConstantData;
import pro.com.data.IOUtils;
import pro.com.data.Mode;
import pro.com.data.MyContact;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

public class CallReciever extends BroadcastReceiver {

	private AudioManager am;
	private final String PREFS_NAME = "CALL_PRIORITY";
	private SharedPreferences shPreferences;
	int i;

	@Override
	public void onReceive(Context context, Intent intent) {

		shPreferences = context.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);

		i = shPreferences.getInt("CallRecevier", 1);
		Log.e("value", "" + i);
		if (i == 2) {
			boolean isAnyModeActive = false;
			MyContact incomingContact = null;

			am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

			ConstantData.ringerModeFilteringOff = am.getRingerMode();

			try {
				if (ConstantData.isFilteringOn) {
					Bundle bundle = intent.getExtras();

					if (null == bundle)
						return;

					Log.i("IncomingCallReceiver", bundle.toString());

					String state = bundle
							.getString(TelephonyManager.EXTRA_STATE);

					Log.i("IncomingCallReceiver", "State: " + state);

					if (state
							.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING)) {

						ConstantData.ringerModeFilteringOn = am.getRingerMode();

						String phonenumber = bundle
								.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
						phonenumber = removeExtraCharacters(phonenumber);
						phonenumber = phonenumber
								.substring(phonenumber.length() - 10,
										phonenumber.length());
						Log.e("Call Reciever=======", phonenumber);

						if (MainActivityTest.db == null) {
							MainActivityTest.db = new DBHelper(context);
						}

						if (MainActivityTest.db != null
								&& !MainActivityTest.db.isOpen()) {
							MainActivityTest.db.open();
						}

						ArrayList<Mode> modeList = MainActivityTest.db
								.fetchModeList();

						Log.e("AAAAAAAAAAAAAAAAAAAAAAAAAAA", "db is not null");

						for (int i = 1; i < modeList.size(); i++) {
							if (isModeActive(modeList.get(i))) {
								isAnyModeActive = true;
								break;
							}
						}

						// to see if general mode is active or not
						try {
							ArrayList<MyContact> contactListGeneral = MainActivityTest.db
									.fetchContactListByMode("General");
							for (int i = 0; i < contactListGeneral.size(); i++) {
								String contactNumber = contactListGeneral
										.get(i).getPhoneNumber();
								try {
									contactNumber = removeExtraCharacters(contactNumber);
									contactNumber = contactNumber.substring(
											contactNumber.length() - 10,
											contactNumber.length());
								} catch (Exception e) {
									Log.e("Cal Reciever", e.toString());
								}

								if (isContactActive(contactListGeneral.get(i))) {
									isAnyModeActive = true;
									break;
								}

							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						if (!isAnyModeActive) {
							// AudioManager am;
							// am = (AudioManager)
							// context.getSystemService(Context.AUDIO_SERVICE);
							// am.setRingerMode(ConstantData.ringerModeFilteringOn);
						} else {
							// AudioManager am;
							// am = (AudioManager)
							// context.getSystemService(Context.AUDIO_SERVICE);
							// am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
						}

						ArrayList<MyContact> contactList = MainActivityTest.db
								.fetchContactList();
						for (int i = 0; i < contactList.size(); i++) {
							String contactNumber = contactList.get(i)
									.getPhoneNumber();
							try {
								contactNumber = removeExtraCharacters(contactNumber);
								contactNumber = contactNumber.substring(
										contactNumber.length() - 10,
										contactNumber.length());
							} catch (Exception e) {
								Log.e("Cal Reciever_C", e.toString());
							}

							if (phonenumber.equalsIgnoreCase(contactNumber)) {
								if (isContactActive(contactList.get(i))) {
									// if(contactList.get(i).getRingerOption().equalsIgnoreCase("Vibrate")){
									// AudioManager am;
									// am = (AudioManager)
									// context.getSystemService(Context.AUDIO_SERVICE);
									// am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
									// }else{
									// AudioManager am;
									// am = (AudioManager)
									// context.getSystemService(Context.AUDIO_SERVICE);
									// am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
									//
									// Log.v("Call Receiver ","......@@@@@@@@@.......");
									// }
									incomingContact = contactList.get(i);
								}
							}
						}

						// Set Final Ringer Position
						if (incomingContact != null) {
							if (incomingContact.getRingerOption()
									.equalsIgnoreCase("Vibrate")) {
								Log.e("here", "Vibrate");
								AudioManager am;
								am = (AudioManager) context
										.getSystemService(Context.AUDIO_SERVICE);
								am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
							} else {
								Log.e("here", "Normal");
								AudioManager am;
								am = (AudioManager) context
										.getSystemService(Context.AUDIO_SERVICE);
								am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
							}
						} else if (isAnyModeActive) {
							Log.e("here", "Silent");
							AudioManager am;
							am = (AudioManager) context
									.getSystemService(Context.AUDIO_SERVICE);
							am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
						}

					} else {
						Log.e("here", "ModeOn");
						AudioManager am;
						am = (AudioManager) context
								.getSystemService(Context.AUDIO_SERVICE);
						am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
						// am.setRingerMode(ConstantData.ringerModeFilteringOn);
					}
				} else {
					Log.e("here", "ModeOff");
					AudioManager am;
					am = (AudioManager) context
							.getSystemService(Context.AUDIO_SERVICE);
					am.setRingerMode(ConstantData.ringerModeFilteringOff);
				}

				if (MainActivityTest.db != null && MainActivityTest.db.isOpen()) {
					MainActivityTest.db.close();
				}
			} catch (Exception e) {
				Log.e("Cal Reciever", e.toString());
			}
		}

	}

	private String removeExtraCharacters(String phoneNumber) {
		try {
			phoneNumber = phoneNumber.replace("(", "");
			phoneNumber = phoneNumber.replace(")", "");
			phoneNumber = phoneNumber.replace("-", "");
			phoneNumber = phoneNumber.replace(" ", "");
			phoneNumber = phoneNumber.replace("+", "");
			phoneNumber = phoneNumber.substring(phoneNumber.length() - 10,
					phoneNumber.length());
		} catch (Exception e) {
			Log.e("Cal Reciever_R", e.toString());
		}
		return phoneNumber;
	}

	private boolean isModeActive(Mode mode) {

		boolean isModeAactive = false;
		Date[] dateArray;

		try {
			Calendar calendar = Calendar.getInstance();
			int i = calendar.get(Calendar.DAY_OF_WEEK);
			i = i - 1;
			String startDayString = mode.getDay();
			String endDayString = getEndDaysString(mode);

			int[] startDayArray = IOUtils
					.getIntArrayFromDayString(startDayString);
			int[] endDayArray = IOUtils.getIntArrayFromDayString(endDayString);

			if (startDayArray[i] == 1 || endDayArray[i] == 1) {

				SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
				Date dateStart = null, dateEnd = null, dateCurrent;
				try {
					dateStart = dateFormat.parse(mode.getStartTime());
					dateEnd = dateFormat.parse(mode.getEndTime());

					Calendar calendar2 = Calendar.getInstance();
					dateCurrent = calendar2.getTime();

					dateStart.setDate(dateCurrent.getDate());
					dateStart.setMonth(dateCurrent.getMonth());
					dateStart.setYear(dateCurrent.getYear());

					dateEnd.setDate(dateCurrent.getDate());
					dateEnd.setMonth(dateCurrent.getMonth());
					dateEnd.setYear(dateCurrent.getYear());

					dateArray = getRealDates(mode.getStartTime(),
							mode.getEndTime(), dateStart, dateEnd,
							startDayArray[i], endDayArray[i]);

					dateStart = dateArray[0];
					dateEnd = dateArray[1];

					boolean isAfter = dateCurrent.after(dateStart);
					boolean isBefore = dateCurrent.before(dateEnd);

					if (isBefore && isAfter) {
						if (MainActivityTest.db.fetchContactListByMode(mode
								.getModeName()) != null
								&& MainActivityTest.db.fetchContactListByMode(
										mode.getModeName()).size() != 0) {
							isModeAactive = true;
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return isModeAactive;
	}

	private boolean isContactActive(MyContact myContact) {

		boolean isContactAactive = false;
		Date[] dateArray;

		Calendar calendar = Calendar.getInstance();
		int i = calendar.get(Calendar.DAY_OF_WEEK);
		i = i - 1;
		String startDayString = MainActivityTest.db.fetchModeByName(
				myContact.getModeName()).getDay();
		String endDayString = getEndDaysString(myContact);

		int[] startDayArray = IOUtils.getIntArrayFromDayString(startDayString);
		int[] endDayArray = IOUtils.getIntArrayFromDayString(endDayString);

		if (startDayArray[i] == 1 || endDayArray[i] == 1) {

			SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
			Date dateStart = null, dateEnd = null, dateCurrent;
			try {
				dateStart = dateFormat.parse(myContact.getStartTime());
				dateEnd = dateFormat.parse(myContact.getEndTime());

				Calendar calendar2 = Calendar.getInstance();
				dateCurrent = calendar2.getTime();

				dateStart.setDate(dateCurrent.getDate());
				dateStart.setMonth(dateCurrent.getMonth());
				dateStart.setYear(dateCurrent.getYear());

				dateEnd.setDate(dateCurrent.getDate());
				dateEnd.setMonth(dateCurrent.getMonth());
				dateEnd.setYear(dateCurrent.getYear());

				dateArray = getRealDates(myContact.getStartTime(),
						myContact.getEndTime(), dateStart, dateEnd,
						startDayArray[i], endDayArray[i]);

				dateStart = dateArray[0];
				dateEnd = dateArray[1];

				boolean isAfter = dateCurrent.after(dateStart);
				boolean isBefore = dateCurrent.before(dateEnd);

				if (isBefore && isAfter) {
					isContactAactive = true;
				}

			} catch (Exception e) {

			}
		}

		return isContactAactive;
	}

	private String getEndDaysString(MyContact myContact) {

		String endDaysString = "";
		String startDaysString = MainActivityTest.db.fetchModeByName(
				myContact.getModeName()).getDay();
		int[] startDaysArray;
		int[] endDaysArray;
		String startTtime = myContact.getStartTime();
		String endTime = myContact.getEndTime();
		String startZoneString = "";
		String endZoneString = "";
		boolean nextDay = false;
		SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
		Date dateStart = null, dateEnd = null;
		try {
			dateStart = dateFormat.parse(startTtime);
			dateEnd = dateFormat.parse(endTime);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (startTtime.contains("AM")) {
			startZoneString = "AM";
		} else {
			startZoneString = "PM";
		}

		if (endTime.contains("AM")) {
			endZoneString = "AM";
		} else {
			endZoneString = "PM";
		}

		startDaysArray = IOUtils.getIntArrayFromDayString(startDaysString);

		if (startZoneString.equalsIgnoreCase("AM")
				&& endZoneString.equalsIgnoreCase("AM")) {
			if (dateStart.before(dateEnd)) {
				nextDay = false;
			} else {
				nextDay = true;
			}
		} else if (startZoneString.equalsIgnoreCase("AM")
				&& endZoneString.equalsIgnoreCase("PM")) {
			nextDay = false;
		} else if (startZoneString.equalsIgnoreCase("PM")
				&& endZoneString.equalsIgnoreCase("AM")) {
			nextDay = true;
		} else if (startZoneString.equalsIgnoreCase("PM")
				&& endZoneString.equalsIgnoreCase("PM")) {
			if (dateStart.before(dateEnd)) {
				nextDay = false;
			} else {
				nextDay = true;
			}
		}

		if (nextDay) {
			int temp[] = new int[7];
			for (int i = 0; i < startDaysArray.length; i++) {
				if (startDaysArray[i] == 1) {
					if (i == 6) {
						temp[0] = 1;
					} else {
						temp[i + 1] = 1;
					}

				} else {
					if (i == 6) {
						temp[0] = 0;
						;
					} else {
						temp[i + 1] = 0;
						;
					}
				}
			}
			endDaysArray = temp;

			endDaysString = IOUtils.getDayStringFromIntArray(endDaysArray);
		} else {
			endDaysString = startDaysString;
		}

		return endDaysString;
	}

	private String getEndDaysString(Mode mode) {

		String endDaysString = "";
		String startDaysString = mode.getDay();
		int[] startDaysArray;
		int[] endDaysArray;
		String startTtime = mode.getStartTime();
		String endTime = mode.getEndTime();
		String startZoneString = "";
		String endZoneString = "";
		boolean nextDay = false;
		SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
		Date dateStart = null, dateEnd = null;
		try {
			dateStart = dateFormat.parse(startTtime);
			dateEnd = dateFormat.parse(endTime);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (startTtime.contains("AM")) {
			startZoneString = "AM";
		} else {
			startZoneString = "PM";
		}

		if (endTime.contains("AM")) {
			endZoneString = "AM";
		} else {
			endZoneString = "PM";
		}

		startDaysArray = IOUtils.getIntArrayFromDayString(startDaysString);

		if (startZoneString.equalsIgnoreCase("AM")
				&& endZoneString.equalsIgnoreCase("AM")) {
			if (dateStart.before(dateEnd)) {
				nextDay = false;
			} else {
				nextDay = true;
			}
		} else if (startZoneString.equalsIgnoreCase("AM")
				&& endZoneString.equalsIgnoreCase("PM")) {
			nextDay = false;
		} else if (startZoneString.equalsIgnoreCase("PM")
				&& endZoneString.equalsIgnoreCase("AM")) {
			nextDay = true;
		} else if (startZoneString.equalsIgnoreCase("PM")
				&& endZoneString.equalsIgnoreCase("PM")) {
			if (dateStart.before(dateEnd)) {
				nextDay = false;
			} else {
				nextDay = true;
			}
		}

		if (nextDay) {
			int temp[] = new int[7];
			for (int i = 0; i < startDaysArray.length; i++) {
				if (startDaysArray[i] == 1) {
					if (i == 6) {
						temp[0] = 1;
					} else {
						temp[i + 1] = 1;
					}

				} else {
					if (i == 6) {
						temp[0] = 0;
						;
					} else {
						temp[i + 1] = 0;
						;
					}
				}
			}
			endDaysArray = temp;

			endDaysString = IOUtils.getDayStringFromIntArray(endDaysArray);
		} else {
			endDaysString = startDaysString;
		}

		return endDaysString;
	}

	private Date[] getRealDates(String startTime, String endTime,
			Date dateStart, Date dateEnd, int startDay, int endDay) {
		Date[] dateArray = new Date[2];
		dateArray[0] = dateStart;
		dateArray[1] = dateEnd;
		String startZoneString = "";
		String endZoneString = "";
		boolean nextDay = false;
		Calendar cal = Calendar.getInstance();
		Date currentDate = cal.getTime();

		if (startTime.contains("AM")) {
			startZoneString = "AM";
		} else {
			startZoneString = "PM";
		}

		if (endTime.contains("AM")) {
			endZoneString = "AM";
		} else {
			endZoneString = "PM";
		}

		if (startZoneString.equalsIgnoreCase("AM")
				&& endZoneString.equalsIgnoreCase("AM")) {
			if (dateStart.before(dateEnd)) {
				nextDay = false;
			} else {
				nextDay = true;
			}
		} else if (startZoneString.equalsIgnoreCase("AM")
				&& endZoneString.equalsIgnoreCase("PM")) {
			nextDay = false;
		} else if (startZoneString.equalsIgnoreCase("PM")
				&& endZoneString.equalsIgnoreCase("AM")) {
			nextDay = true;
		} else if (startZoneString.equalsIgnoreCase("PM")
				&& endZoneString.equalsIgnoreCase("PM")) {
			if (dateStart.before(dateEnd)) {
				nextDay = false;
			} else {
				nextDay = true;
			}
		}

		if (nextDay) {
			long _24Hours = 24 * 60 * 60 * 1000;
			long newStartTimeMillis, newEndTimeMillis;
			long startTimeMillis = dateStart.getTime();
			long endTimeMillis = dateEnd.getTime();

			// difference = endTimeMillis - startTimeMillis;
			// difference2 = _24Hours - difference;
			if (endDay == 1 && currentDate.before(dateStart)) {
				newStartTimeMillis = startTimeMillis - _24Hours;
				dateArray[0] = new Date(newStartTimeMillis);
			} else {
				newEndTimeMillis = endTimeMillis + _24Hours;
				dateArray[1] = new Date(newEndTimeMillis);
			}

		} else {
		}

		return dateArray;
	}
}
