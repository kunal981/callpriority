package pro.com;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import pro.com.data.GeneralContact;
import pro.com.data.Mode;
import pro.com.data.MyContact;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This is the DB Helper class which mainly contains the Database related
 * functionalities, Once the Object of this class created it will create an
 * database if it not exists other- wise it open the existing database
 * 
 * @author aakhani
 * 
 */

public class DBHelper {

	// used to define db name
	private static final String DATABASE_NAME = "CALLPRIORITY";
	// used to define db version
	private static final int DATABASE_VERSION = 1;

	// Variable which defines the table names
	private static final String TABLE_NAME_MODES = "tblModes";
	private static final String TABLE_NAME_CONTACTS = "tblContatcts";
	// private static final String TABLE_NAME_GROUPCONTACTS =
	// "tblGroupContatcts";
	private static final String TABLE_NAME_GENERAL_CONTACTS = "tblGeneralContatcts";

	private static final String CREATE_TABLE_MODES = "create table tblModes (id integer primary key autoincrement,"
			+ "modename text,"
			+ "starttime text,"
			+ "endtime text,"
			+ "day text," + "ringeroption text," + "modenumber integer);";

	private static final String CREATE_TABLE_CONTACTS = "create table tblContatcts (id integer primary key autoincrement,"
			+ "contactname text,"
			+ "starttime text,"
			+ "endtime text,"
			+ "ringeroption text," + "modename text," + "phonenumber text);";

	// private static final String CREATE_TABLE_GROUPCONTACTS =
	// "create table tblGroupContatcts (id integer primary key autoincrement,"
	// + "groupcontactname text,"
	// + "starttime text,"
	// + "endtime text,"
	// + "ringeroption text," + "modename text," + "phonenumber text);";
	//

	private static final String CREATE_TABLE_GENERAL_CONTACTS = "create table tblGeneralContatcts (id integer primary key autoincrement,"
			+ "contactid integer," + "timestamp text);";

	// used to store current context
	private final Context context;
	// define object of helper class
	private DatabaseHelper dataHelper;
	// define object of SQLiteDatabase
	private SQLiteDatabase db;
	// used to store path of db
	private static String DB_PATH = "";

	private boolean isOpen;

	// class constructor
	public DBHelper(Context ctx) {

		this.context = ctx;
		dataHelper = new DatabaseHelper(context);
		DB_PATH = "/data/data/" + ctx.getPackageName() + "/databases/";
		Log.e("DB_PATH", "" + DB_PATH);

	}

	/**
	 * This class is used to create or copy(from assets)
	 * 
	 * @author kmevada
	 */

	private static class DatabaseHelper extends SQLiteOpenHelper {

		// used to store current context
		Context myContext = null;

		// class constructor
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			this.myContext = context;
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			try {
				db.execSQL(CREATE_TABLE_MODES);
				db.execSQL("insert into "
						+ TABLE_NAME_MODES
						+ "(modename,modenumber,day,starttime,endtime) values ('General',0,'Su M Tu W Th F Sa','00:01 AM','11:59 PM');");
				db.execSQL(CREATE_TABLE_CONTACTS);

				db.execSQL(CREATE_TABLE_GENERAL_CONTACTS);

				Log.e("before", "before");

				db.execSQL("alter table "
						+ TABLE_NAME_CONTACTS
						+ " add column groupId varchar(50) not null default '-1'");
				db.execSQL("alter table "
						+ TABLE_NAME_GENERAL_CONTACTS
						+ " add column groupId varchar(50) not null default '-1'");

				Log.e("after", "after");

			} catch (Exception e) {
				Log.e("Exception in create query", e.toString());
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			Log.w("DBHelper", "Upgrading database from version " + oldVersion
					+ " to " + newVersion + ", which will destroy all old data");

			// remove old table first and then create tables
			onCreate(db);
		}

		/**
		 * This function is used to check whether file is exist or not
		 */
		private boolean isDataBaseExist() {

			File dbFile = new File(DB_PATH + DATABASE_NAME);
			return dbFile.exists();
		}
	}

	public void createModesRow(String modeName, String startTime,
			String endTime, String day, String ringerOption, int modeNumber) {
		try {
			ContentValues initialValues = new ContentValues();

			initialValues.put("modename", modeName);
			initialValues.put("starttime", startTime);
			initialValues.put("endtime", endTime);
			initialValues.put("day", day);
			initialValues.put("ringeroption", ringerOption);
			initialValues.put("modenumber", modeNumber);

			db.insert(TABLE_NAME_MODES, null, initialValues);

		} catch (Exception e) {
			Log.e("Exception in insert query", e.toString());

		}
	}

	public void updateModesRow(int id, String modeName, String startTime,
			String endTime, String day, String ringerOption, int modeNumber) {
		try {
			ContentValues initialValues = new ContentValues();

			initialValues.put("modename", modeName);
			initialValues.put("starttime", startTime);
			initialValues.put("endtime", endTime);
			initialValues.put("day", day);
			initialValues.put("ringeroption", ringerOption);
			initialValues.put("modenumber", modeNumber);

			db.update(TABLE_NAME_MODES, initialValues, "id=" + id, null);

		} catch (Exception e) {
			Log.e("Exception in insert query", e.toString());

		}
	}

	public void createContatcsRow(String contactName, String startTime,
			String endTime, String ringerOption, String modeName,
			String phoneNumber, String groupId) {
		try {
			ContentValues initialValues = new ContentValues();

			initialValues.put("contactname", contactName);
			initialValues.put("starttime", startTime);
			initialValues.put("endtime", endTime);
			initialValues.put("ringeroption", ringerOption);
			initialValues.put("modename", modeName);
			initialValues.put("phonenumber", phoneNumber);
			initialValues.put("groupId", groupId);

			long contactId = db
					.insert(TABLE_NAME_CONTACTS, null, initialValues);

			// Calendar cl = Calendar.getInstance();
			// long timeStamp = cl.getTimeInMillis();
			// createGeneralContatcsRow((int) contactId,
			// String.valueOf(timeStamp));

		} catch (Exception e) {
			Log.e("Exception in insert query", e.toString());

		}
	}

	public void createGeneralContatcsRow(int contactId, String timeStamp) {
		try {
			ContentValues initialValues = new ContentValues();

			initialValues.put("contactid", contactId);
			initialValues.put("timestamp", timeStamp);

			db.insert(TABLE_NAME_GENERAL_CONTACTS, null, initialValues);

		} catch (Exception e) {
			Log.e("Exception in insert query", e.toString());

		}
	}

	public void updateContactRow(int id, String contactName, String startTime,
			String endTime, String ringerOption, String modeName,
			String phoneNumber) {

		try {
			ContentValues initialValues = new ContentValues();

			initialValues.put("contactname", contactName);
			initialValues.put("starttime", startTime);
			initialValues.put("endtime", endTime);
			initialValues.put("ringeroption", ringerOption);
			initialValues.put("modename", modeName);
			initialValues.put("phonenumber", phoneNumber);

			db.update(TABLE_NAME_CONTACTS, initialValues, "id=" + id, null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Mode> fetchModeList() {

		Cursor c = null;
		ArrayList<Mode> modeList = null;
		try {

			c = db.rawQuery("select * from " + TABLE_NAME_MODES + ";", null);

			if (c != null && c.getCount() > 0) {
				modeList = new ArrayList<Mode>();

				for (int i = 0; i < c.getCount(); i++) {
					c.moveToPosition(i);
					Mode mode = new Mode();
					mode.setId(c.getInt(0));
					mode.setModeName(c.getString(1));
					mode.setStartTime(c.getString(2));
					mode.setEndTime(c.getString(3));
					mode.setDay(c.getString(4));
					mode.setRingerOption(c.getString(5));
					mode.setModeNumber(c.getInt(6));
					modeList.add(mode);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
		}

		return modeList;

	}

	public Mode fetchModeByName(String modeName) {

		Cursor c = null;
		Mode mode = new Mode();
		try {
			c = db.rawQuery("select * from " + TABLE_NAME_MODES
					+ " where modename = '" + modeName + "' ;", null);

			if (c != null && c.getCount() > 0) {
				c.moveToPosition(0);
				mode.setId(c.getInt(0));
				mode.setModeName(c.getString(1));
				mode.setStartTime(c.getString(2));
				mode.setEndTime(c.getString(3));
				mode.setDay(c.getString(4));
				mode.setRingerOption(c.getString(5));
				mode.setModeNumber(c.getInt(6));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
		}

		return mode;

	}

	public ArrayList<Mode> fetchModeListByOrder() {

		Cursor c = null;
		ArrayList<Mode> modeList = null;
		ArrayList<Mode> modeList2 = null;
		try {

			c = db.rawQuery("select * from " + TABLE_NAME_MODES
					+ " order by modenumber asc;", null);

			if (c != null && c.getCount() > 0) {
				modeList = new ArrayList<Mode>();

				for (int i = 0; i < c.getCount(); i++) {
					c.moveToPosition(i);
					Mode mode = new Mode();
					mode.setId(c.getInt(0));
					mode.setModeName(c.getString(1));
					mode.setStartTime(c.getString(2));
					mode.setEndTime(c.getString(3));
					mode.setDay(c.getString(4));
					mode.setRingerOption(c.getString(5));
					mode.setModeNumber(c.getInt(6));
					modeList.add(mode);
				}
			}

			// modeList2 = new ArrayList<Mode>();
			// for(int i=0;i<modeList.size();i++){
			//
			// switch (modeList.get(i).getModeNumber()) {
			// case 0:
			// modeList2.add(0, modeList.get(i));
			// case 1:
			// modeList2.add(1, modeList.get(i));
			// case 2:
			// modeList2.add(2, modeList.get(i));
			// case 3:
			// modeList2.add(3, modeList.get(i));
			// case 4:
			// modeList2.add(4, modeList.get(i));
			// break;
			//
			// default:
			// break;
			// }
			// }

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
		}

		return modeList;

	}

	public ArrayList<MyContact> fetchContactList() {

		Cursor c = null;
		ArrayList<MyContact> contactList = null;
		try {

			c = db.rawQuery("select * from " + TABLE_NAME_CONTACTS + ";", null);

			if (c != null && c.getCount() > 0) {
				contactList = new ArrayList<MyContact>();

				for (int i = 0; i < c.getCount(); i++) {
					c.moveToPosition(i);
					MyContact myContact = new MyContact();
					myContact.setId(c.getInt(0));
					myContact.setContactName(c.getString(1));
					myContact.setStartTime(c.getString(2));
					myContact.setEndTime(c.getString(3));
					myContact.setRingerOption(c.getString(4));
					myContact.setModeName(c.getString(5));
					myContact.setPhoneNumber(c.getString(6));
					contactList.add(myContact);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
		}

		return contactList;

	}

	public ArrayList<GeneralContact> fetchGeneralContactList() {

		Cursor c = null;
		ArrayList<GeneralContact> contactList = null;
		try {

			c = db.rawQuery("select * from " + TABLE_NAME_GENERAL_CONTACTS
					+ ";", null);

			if (c != null && c.getCount() > 0) {
				contactList = new ArrayList<GeneralContact>();

				for (int i = 0; i < c.getCount(); i++) {
					c.moveToPosition(i);
					GeneralContact myContact = new GeneralContact();
					myContact.setId(c.getInt(0));
					myContact.setContactId(c.getInt(1));
					myContact.setTimeStamp(c.getString(2));
					contactList.add(myContact);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
		}

		return contactList;

	}

	public ArrayList<MyContact> fetchContactListByMode(String modeName) {

		Cursor c = null;
		ArrayList<MyContact> contactList = null;
		try {

			c = db.rawQuery("select * from " + TABLE_NAME_CONTACTS
					+ " where modename = '" + modeName + "' ;", null);

			// c=db.query(TABLE_NAME_CONTACTS, new String[]{}, "modename",
			// modeName, null, null, null);

			if (c != null && c.getCount() > 0) {
				contactList = new ArrayList<MyContact>();

				for (int i = 0; i < c.getCount(); i++) {
					c.moveToPosition(i);
					MyContact myContact = new MyContact();
					myContact.setId(c.getInt(0));
					myContact.setContactName(c.getString(1));
					myContact.setStartTime(c.getString(2));
					myContact.setEndTime(c.getString(3));
					myContact.setRingerOption(c.getString(4));
					myContact.setModeName(c.getString(5));
					myContact.setPhoneNumber(c.getString(6));
					myContact.setGroupId(c.getString(7));
					contactList.add(myContact);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
		}

		return contactList;

	}

	public void deleteMode(int modeID) {
		try {
			db.delete(TABLE_NAME_MODES, "id = " + modeID, null);
		} catch (Exception e) {
			Log.e("Exception in deleteMode()", e.toString());
		}
	}

	public void deleteContact(int contactID) {
		try {
			db.delete(TABLE_NAME_CONTACTS, "id = " + contactID, null);
		} catch (Exception e) {
			Log.e("Exception in deleteContact()", e.toString());
		}
	}

	public void deleteGeneralContact(int id, int contactId) {
		try {
			deleteContact(contactId);
			db.delete(TABLE_NAME_GENERAL_CONTACTS, "id = " + id, null);
		} catch (Exception e) {
			Log.e("Exception in deleteContact()", e.toString());
		}
	}

	// public void deleteGeneralContact(int id) {
	// try {
	// // deleteContact(id);
	// db.delete(TABLE_NAME_GENERAL_CONTACTS, "id = " + id, null);
	// } catch (Exception e) {
	// Log.e("Exception in deleteContact()", e.toString());
	// }
	// }
	public void deleteContactByMode(String modeName) {
		modeName = "'" + modeName + "'";
		try {
			db.delete(TABLE_NAME_CONTACTS, "modename=" + modeName, null);
		} catch (Exception e) {
			Log.e("Exception in deleteContact()", e.toString());
		}
	}

	public void updateAllContactsByMode(String modeName, String startTime,
			String endTime, String ringerOption) {

		try {
			ContentValues initialValues = new ContentValues();

			initialValues.put("starttime", startTime);
			initialValues.put("endtime", endTime);
			initialValues.put("ringeroption", ringerOption);

			db.update(TABLE_NAME_CONTACTS, initialValues, "modename='"
					+ modeName + "'", null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateAllContactsForModeNameChange(String oldModeName,
			String newModeName) {

		try {
			ContentValues initialValues = new ContentValues();

			initialValues.put("modename", newModeName);

			db.update(TABLE_NAME_CONTACTS, initialValues, "modename='"
					+ oldModeName + "'", null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method will open database if it is not exist than it will copied
	 * from existing resource file
	 * 
	 * @return
	 * @throws SQLException
	 */

	public DBHelper open() throws SQLException {
		try {
			// check that db is exist or not
			boolean isExist = dataHelper.isDataBaseExist();
			if (isExist == false) {
				db = dataHelper.getWritableDatabase();
				// copy db from assets
				if (db.isOpen())
					db.close();
			}

			db = dataHelper.getWritableDatabase();
			isOpen = true;
		} catch (Exception e) {
			ShowMessage("error", "" + e.toString());
		}
		return this;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void close() {
		// db.close();
	}

	/**
	 * This method is used to fetch list of the provider from the user list.
	 * 
	 * @return
	 */
	// public String[] fetchRegistraionList(int mField){
	//
	// Cursor c=null;
	// String registrationList[]=null;
	// try{
	//
	// c=db.rawQuery("select fieldname from "+TABLE_NAME_REGISTRATION+" where selectedfield = '"+mField+"';",null);
	//
	// if(c!=null && c.getCount()>0){
	// registrationList=new String[c.getCount()];
	// //registrationList[0]=context.getString(R.string.lblProvider);
	// for(int i=0;i<c.getCount();i++){
	// c.moveToPosition(i);
	// registrationList[i]=c.getString(0);
	//
	// }
	// }/*else{
	// providerName=new String[1];
	// providerName[0]=context.getString(R.string.lblProvider);
	// }*/
	//
	//
	// }catch (Exception e) {
	// e.printStackTrace();
	// }finally {
	// if(c!=null) c.close();
	// }
	//
	//
	// return registrationList;
	//
	// }

	// public int getFieldValue(String fieldString,int fieldGroupID){
	// int a = 0;
	// Cursor c=null;
	// try{
	// c =
	// db.rawQuery("select RecNo from "+TABLE_NAME_REGISTRATION+" where selectedfield="+fieldGroupID+" and fieldname='"+fieldString+"';",null);
	// c.moveToFirst();
	// a = c.getInt(0);
	// }catch(Exception e){
	// Log.e("Error in getFieldValue()", e.toString());
	// }finally{
	// if(c!=null) c.close();
	// }
	// return a;
	// }
	//
	// public int getGroupID(String fieldString){
	// int a = 0;
	// Cursor c = null;
	// try {
	// c =
	// db.rawQuery("select selectedfield from "+TABLE_NAME_REGISTRATION+" where fieldname='"+fieldString+"';",null);
	// c.moveToFirst();
	// a = c.getInt(0);
	// } catch (Exception e) {
	// Log.e("Error in getFieldValue()", e.toString());
	// }finally{
	// if(c!=null) c.close();
	// }
	// return a;
	// }

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		if (db.isOpen())
			db.close();
	}

	// to display message
	private void ShowMessage(String title, String message) {
		AlertDialog.Builder b = new AlertDialog.Builder(this.context);
		AlertDialog a = b.create();
		a.setTitle(title);
		a.setMessage(message);
		a.setButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
			}
		});

		a.show();
	}

}
