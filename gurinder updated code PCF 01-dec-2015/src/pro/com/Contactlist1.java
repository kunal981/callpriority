/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pro.com;

import java.util.ArrayList;
import java.util.List;

import pro.com.GroupsContacts.GroupAdapter;
import pro.com.data.Mode;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.GroupMembership;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

public final class Contactlist1 extends Activity {

	public static final String TAG = "ContactManager";

	// private Button mAddAccountButton;
	private ListView mContactList;
	// private boolean mShowInvisible;
	// private CheckBox mShowInvisibleControl;
	public static DBHelper db;
	EditText editsearch;
	private TextView txtHeader;
	Button btngroup, btn_contacts;
	// ArrayAdapter<String> adapter;
	private String currentMode;

	private String phoneNumber;
	private String id[];
	private ArrayList<Integer> sid;
	private String startTimeExtra, endTimeExtra, ringTypeExtra;

	ArrayList<String> name;
	ArrayList<String> no;
	// ArrayList<String> id;
	ContactAdapter contactAdapter;
	ArrayList<String> tittle;

	/**
	 * Called when the activity is first created. Responsible for initializing
	 * the UI.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.v(TAG, "Activity State: onCreate()");
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main3);

		sid = new ArrayList<Integer>();

		editsearch = (EditText) findViewById(R.id.editsearch);
		btngroup = (Button) findViewById(R.id.btn_groups);
		btn_contacts = (Button) findViewById(R.id.btn_contacts);

		currentMode = getIntent().getExtras().getString("currentMode");
		startTimeExtra = getIntent().getExtras().getString("start_time");
		endTimeExtra = getIntent().getExtras().getString("end_time");
		ringTypeExtra = getIntent().getExtras().getString("ring_type");

		// Log.e("currentModeContactList", "" + currentMode);
		// Log.e("start_time", "" + startTimeExtra);
		// Log.e("end_time", "" + endTimeExtra);
		// Log.e("ring_type", "" + ringTypeExtra);
		//
		// Obtain handles to UI objects

		mContactList = (ListView) findViewById(R.id.ListView01);
		name = new ArrayList<String>();
		no = new ArrayList<String>();
		// id = new ArrayList<String>();
		contactAdapter = new ContactAdapter(getApplicationContext(),
				getLayoutInflater(), name);

		mContactList.setTextFilterEnabled(true);
		mContactList.setAdapter(contactAdapter);

		populateContactList();

		btn_contacts.setBackground(getResources().getDrawable(
				R.drawable.btn_slc1t));
		btn_contacts.setPadding(0, 0, 0, 22);

		mContactList.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> a, View v, int position,
					long name) {

				Intent i12 = new Intent(Contactlist1.this, ContactSetting.class);
				Cursor cphon;
				// Log.e("sid:",""+ sid.size());
				if (sid.size() < id.length && sid.size() > 0) {
					cphon = getContentResolver().query(
							ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ "=" + id[sid.get(position)], null, null);
				} else {

					cphon = getContentResolver().query(
							ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ "=" + id[position], null, null);
				}

				while (cphon.moveToNext()) {

					phoneNumber = cphon.getString(cphon
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

				}
				Log.e("phoneNumber:", phoneNumber);
				String str1 = (String) mContactList.getItemAtPosition(position);

				// Log.e("str1:", str1);

				Bundle bdl1 = new Bundle();
				bdl1.putString("str1", str1);
				bdl1.putString("currentMode", currentMode);
				bdl1.putString("phoneNumber", phoneNumber);
				bdl1.putString("start_time", startTimeExtra);
				bdl1.putString("end_time", endTimeExtra);
				bdl1.putString("ring_type", ringTypeExtra);
				i12.putExtras(bdl1);
				startActivityForResult(i12, 1);

			}
		});

		editsearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

				// editsearch.getText().toString().toLowerCase(Locale.getDefault());
				//
				// contactAdapter.filter(text);
				contactAdapter.getFilter().filter(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		btngroup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btngroup.setBackground(getResources().getDrawable(
						R.drawable.btn_slc1t));
				// btngroup.setPadding(0, 0, 0, 22);
				btn_contacts.setBackground(getResources().getDrawable(
						R.drawable.btn));

				Intent it = new Intent(Contactlist1.this, GroupsContacts.class);

				it.putExtra("currentMode", currentMode);

				startActivity(it);

				// name.clear();
				//
				// tittle = new ArrayList<String>();
				//
				// contactAdapter = new ContactAdapter(getApplicationContext(),
				// getLayoutInflater(), tittle);
				// Log.e("", ""+tittle);
				// contactAdapter.notifyDataSetChanged();
				//

			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		btngroup.setBackground(getResources().getDrawable(R.drawable.btn));
		btn_contacts.setBackground(getResources().getDrawable(
				R.drawable.btn_slc1t));
		btn_contacts.setPadding(0, 0, 0, 22);

	}

	/**
	 * Populate the contact list based on account currently selected in the
	 * account spinner.
	 */
	private void populateContactList() {
		// Build adapter with contact entries
		Cursor cursor = getContacts();
		int i = 0;
		// String name[] = new String[cursor.getCount()];

		id = new String[cursor.getCount()];

		String group[] = new String[cursor.getCount()];

		if (cursor.getCount() > 0) {
			while (cursor.moveToNext()) {

				name.add(cursor.getString(cursor
						.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));

				// id.add(cursor.getString(cursor
				// .getColumnIndex(ContactsContract.Contacts._ID)));

				// name[i] = cursor
				// .getString(cursor
				// .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

				id[i] = cursor.getString(cursor
						.getColumnIndex(ContactsContract.Contacts._ID));

				// group[i]=cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.GroupMembership.GROUP_ROW_ID));

				i++;
			}
		}
		String[] fields = new String[] { ContactsContract.Data.DISPLAY_NAME };

		// SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
		// R.layout.main3, cursor,
		// fields, new int[]{R.id.contactEntryText});

		// ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
		// android.R.layout.simple_list_item_1, name);

		// mContactList.setAdapter(adapter);

	}

	/**
	 * Obtains the contact list for the currently selected account.
	 * 
	 * @return A cursor for for accessing the contact list.
	 */
	private Cursor getContacts() {
		// Run query
		Uri uri = ContactsContract.Contacts.CONTENT_URI;
		String[] projection = new String[] { ContactsContract.Contacts._ID,
				ContactsContract.Contacts.DISPLAY_NAME };

		String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER;

		String[] selectionArgs = null;

		String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
				+ " COLLATE LOCALIZED ASC";

		// String[] projectionGroup = new String[]{
		// ContactsContract.CommonDataKinds.GroupMembership.GROUP_ROW_ID ,
		// ContactsContract.CommonDataKinds.GroupMembership.CONTACT_ID};

		return managedQuery(uri, projection, selection, selectionArgs,
				sortOrder);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		setResult(RESULT_OK, data);
		// Intent it=new Intent(Contactlist1.this,ContactSetting.class);
		// startActivity(it);
		finish();
	}

	class ContactAdapter extends BaseAdapter {
		Context mContext;
		LayoutInflater inflater;
		ArrayList<String> mArrayList = new ArrayList<String>();
		private ArrayList<String> originalData = null;
		private ItemFilter mFilter = new ItemFilter();

		public ContactAdapter(Context mContext, LayoutInflater inflater,
				ArrayList<String> ArrayList) {
			super();
			this.mContext = mContext;
			this.inflater = inflater;
			this.mArrayList = ArrayList;
			originalData = ArrayList;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mArrayList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mArrayList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			final ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();

				convertView = inflater
						.inflate(R.layout.contactlist_items, null);

				holder.textview = (TextView) convertView
						.findViewById(R.id.text1);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.textview.setText(mArrayList.get(position));

			return convertView;
		}

		public class ViewHolder {

			TextView textview;
		}

		// // Filter Class
		// public void filter(String charText) {
		// charText = charText.toLowerCase(Locale.getDefault());
		// mArrayList.clear();
		//
		// int count = mArrayList.size();
		//
		// if (charText.length() == 0) {
		// mArrayList.addAll(name);
		// } else {
		//
		// // for (mArrayList wp : name) {
		// // if (wp.toLowerCase(Locale.getDefault()).contains(charText)) {
		// // list.add(wp);
		// // }
		// // }
		// for (int i = 0; i < count; i++) {
		// charText = mArrayList.get(i);
		// if (charText.toLowerCase().contains(charText)) {
		// mArrayList.add(charText);
		// }
		// }
		// }
		// notifyDataSetChanged();
		// }
		// }
		//
		// // private class ItemFilter extends Filter {
		// // @Override
		// // protected FilterResults performFiltering(CharSequence constraint)
		// {
		// //
		// // String filterString = constraint.toString().toLowerCase();
		// //
		// // FilterResults results = new FilterResults();
		// //
		// // final List<String> list = originalData;
		// //
		// // int count = list.size();
		// // final ArrayList<String> nlist = new ArrayList<String>(count);
		// //
		// // String filterableString ;
		// //
		// // for (int i = 0; i < count; i++) {
		// // filterableString = list.get(i);
		// // if (filterableString.toLowerCase().contains(filterString)) {
		// // nlist.add(filterableString);
		// // }
		// // }
		// //
		// // results.values = nlist;
		// // results.count = nlist.size();
		// //
		// // return results;
		// // }
		// //
		// // @Override
		// // protected void publishResults(CharSequence constraint,
		// // FilterResults results) {
		// // // TODO Auto-generated method stub
		// //
		// // }
		// // }

		public Filter getFilter() {
			// TODO Auto-generated method stub
			return mFilter;
		}

		private class ItemFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {

				String filterString = constraint.toString().toLowerCase();

				FilterResults results = new FilterResults();

				final List<String> list = originalData;

				int count = list.size();
				final ArrayList<String> nlist = new ArrayList<String>(count);

				sid = new ArrayList<Integer>(count);

				String filterableString;

				for (int i = 0; i < count; i++) {
					filterableString = list.get(i);
					if (filterableString.toLowerCase().contains(filterString)) {
						nlist.add(filterableString);
						sid.add(i);
					}
				}

				results.values = nlist;
				results.count = nlist.size();

				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				mArrayList = (ArrayList<String>) results.values;
				// Log.e("mArrayList", "" + mArrayList);
				notifyDataSetChanged();
			}

		}
	}
}
