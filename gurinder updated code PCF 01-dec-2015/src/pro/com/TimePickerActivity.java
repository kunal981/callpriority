package pro.com;

import java.util.Calendar;
import java.util.Locale;

import pro.com.data.IOUtils;
import pro.com.data.Mode;
import pro.com.wheelpicker.ArrayWheelAdapter;
import pro.com.wheelpicker.NumericWheelAdapter;
import pro.com.wheelpicker.WheelView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class TimePickerActivity extends Activity {

	private Button btnOk, btnCancel;
	private WheelView hours;
	private WheelView mins;
	private WheelView ampm;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.time_picker);

		btnOk = (Button) findViewById(R.id.button1);
		btnCancel = (Button) findViewById(R.id.button2);
		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();

				String hoursString = IOUtils.getTimeString((hours
						.getCurrentItem() + 1));
				String minssString = IOUtils.getTimeString(mins
						.getCurrentItem());
				intent.putExtra("hours", hoursString);
				intent.putExtra("mins", minssString);
				if (ampm.getCurrentItem() == 0) {
					intent.putExtra("ampm", "AM");
				} else {
					intent.putExtra("ampm", "PM");
				}
				setResult(RESULT_OK, intent);
				finish();
			}
		});

		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		hours = (WheelView) findViewById(R.id.hour);
		NumericWheelAdapter hourAdapter = new NumericWheelAdapter(this, 1, 12);
		hourAdapter.setItemResource(R.layout.wheel_text_item);
		hourAdapter.setItemTextResource(R.id.text);
		hours.setViewAdapter(hourAdapter);

		mins = (WheelView) findViewById(R.id.mins);
		NumericWheelAdapter minAdapter = new NumericWheelAdapter(this, 0, 59,
				"%02d");
		minAdapter.setItemResource(R.layout.wheel_text_item);
		minAdapter.setItemTextResource(R.id.text);
		mins.setViewAdapter(minAdapter);
		mins.setCyclic(true);

		ampm = (WheelView) findViewById(R.id.ampm);
		ArrayWheelAdapter<String> ampmAdapter = new ArrayWheelAdapter<String>(
				this, new String[] { "AM", "PM" });
		ampmAdapter.setItemResource(R.layout.wheel_text_item);
		ampmAdapter.setItemTextResource(R.id.text);
		ampm.setViewAdapter(ampmAdapter);

		// String startTime = getIntent().getExtras().getString("startTime");
		//
		// String[] parts = new String[3];
		//
		// parts = startTime.split("-");
		//
		// String part1 = parts[0];
		// String part2 = parts[1];
		// String part3 = parts[2];
		//
		// int h1 = Integer.parseInt(part1);
		// int h2 = Integer.parseInt(part2);
		// int h3 = Integer.parseInt(part3);
		//
		// // set current time
		// Calendar calendar = Calendar.getInstance(Locale.US);
		// hours.setCurrentItem(h1);
		// mins.setCurrentItem(h2);
		// ampm.setCurrentItem(h3);

		// set current time
		Calendar calendar = Calendar.getInstance(Locale.US);

		String ptime = getIntent().getExtras().getString("time");

		int phours = 0;
		int pmins = 0;
		int pampm = 0;
		if (ptime.equals("none")) {
			phours = calendar.get(Calendar.HOUR) - 1;
			pmins = calendar.get(Calendar.MINUTE);
			pampm = calendar.get(Calendar.AM_PM);
		} else {
			String dotSplitEnd[] = new String[2];

			dotSplitEnd = ptime.split(":");

			String apSplitEnd[] = new String[2];

			apSplitEnd = dotSplitEnd[1].split(" ");

			pmins = Integer.parseInt(apSplitEnd[0]);
			phours = Integer.parseInt(dotSplitEnd[0]) - 1;

			if (apSplitEnd[1].equals("AM")) {
				pampm = 0;
			} else {
				pampm = 1;
			}
		}
		hours.setCurrentItem(phours);
		mins.setCurrentItem(pmins);
		ampm.setCurrentItem(pampm);

		Log.e("timepicker", "" + calendar.get(Calendar.PM));
	}

}
