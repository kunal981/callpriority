package pro.com.data;

public class IOUtils {

	public static String getDayStringFromIntArray(int[] checkBoxValues) {

		String dayString = "";
		boolean comma = false;

		for (int i = 0; i < checkBoxValues.length; i++) {
			switch (i) {

			case 0:
				if (checkBoxValues[i] == 1) {
					dayString = dayString + "Su";
					comma = true;
				}
				break;
			case 1:
				if (checkBoxValues[i] == 1) {
					if (comma) {
						dayString = dayString + " ";
					}
					dayString = dayString + "M";
					comma = true;
				}
				break;
			case 2:

				if (checkBoxValues[i] == 1) {
					if (comma) {
						dayString = dayString + " ";
					}
					dayString = dayString + "Tu";
					comma = true;
				}
				break;
			case 3:

				if (checkBoxValues[i] == 1) {
					if (comma) {
						dayString = dayString + " ";
					}
					dayString = dayString + "W";
					comma = true;
				}
				break;
			case 4:

				if (checkBoxValues[i] == 1) {
					if (comma) {
						dayString = dayString + " ";
					}
					dayString = dayString + "Th";
					comma = true;
				}
				break;
			case 5:

				if (checkBoxValues[i] == 1) {
					if (comma) {
						dayString = dayString + " ";
					}
					dayString = dayString + "F";
					comma = true;
				}
				break;
			case 6:

				if (checkBoxValues[i] == 1) {
					if (comma) {
						dayString = dayString + " ";
					}
					dayString = dayString + "Sa";
					comma = true;
				}
				break;

			}
		}

		return dayString;
	}

	public static int[] getIntArrayFromDayString(String dayString) {

		int[] checkBoxValues = { 0, 0, 0, 0, 0, 0, 0 };

		try {
			String[] temp = dayString.split(" ");

			for (int i = 0; i < temp.length; i++) {
				if (temp[i].equalsIgnoreCase("Su")) {
					checkBoxValues[0] = 1;
				} else if (temp[i].equalsIgnoreCase("M")) {
					checkBoxValues[1] = 1;
				} else if (temp[i].equalsIgnoreCase("Tu")) {
					checkBoxValues[2] = 1;
				} else if (temp[i].equalsIgnoreCase("W")) {
					checkBoxValues[3] = 1;
				} else if (temp[i].equalsIgnoreCase("Th")) {
					checkBoxValues[4] = 1;
				} else if (temp[i].equalsIgnoreCase("F")) {
					checkBoxValues[5] = 1;
				} else if (temp[i].equalsIgnoreCase("Sa")) {
					checkBoxValues[6] = 1;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return checkBoxValues;
	}

	public static String getTimeString(int time) {
		String timeString = "";

		if (time <= 9) {
			timeString = "0" + String.valueOf(time);
		} else {
			timeString = String.valueOf(time);
		}

		return timeString;
	}
}
