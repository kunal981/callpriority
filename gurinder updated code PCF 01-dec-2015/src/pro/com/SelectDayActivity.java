/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pro.com;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public final class SelectDayActivity extends Activity {

	private ListView listDays;
	private Button btnOK;
	private ArrayList<String> dayList = new ArrayList<String>();
	private MyListAdapter adapter;
	private int[] checkBoxValues = { 0, 0, 0, 0, 0, 0, 0 };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_days);

		listDays = (ListView) findViewById(R.id.ListView01);
		btnOK = (Button) findViewById(R.id.btnOK);

		checkBoxValues = getIntent().getExtras().getIntArray("checkBoxValues");

		dayList.add("Sunday");
		dayList.add("Monday");
		dayList.add("Tuesday");
		dayList.add("Wednesday");
		dayList.add("Thursday");
		dayList.add("Friday");
		dayList.add("Saturday");

		adapter = new MyListAdapter(dayList);
		listDays.setAdapter(adapter);

		btnOK.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent intent = new Intent();
				intent.putExtra("checkBoxValues", checkBoxValues);
				setResult(RESULT_OK, intent);
				finish();
			}
		});

	}

	class MyListAdapter extends BaseAdapter {

		private List<String> datarow = null;
		private CheckBox chkDay;
		private TextView txtDayName;

		public MyListAdapter(List<String> datarow) {
			this.datarow = datarow;
		}

		public int getCount() {
			// TODO Auto-generated method stub
			return datarow.size();
		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return datarow.get(position);
		}

		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {

			View view = null;
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.select_day_row, null);

			chkDay = (CheckBox) view.findViewById(R.id.chkDay);
			txtDayName = (TextView) view.findViewById(R.id.txtDayName);

			txtDayName.setText(datarow.get(position));
			if (checkBoxValues[position] == 1) {
				chkDay.setChecked(true);
			}

			chkDay.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					checkBoxValues[position] = 1;
				}
			});

			return view;
		}

	}

}
