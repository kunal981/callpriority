package pro.com;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

public class BootUpReceiver extends BroadcastReceiver {
	SharedPreferences shPreferences;
	private final String PREFS_NAME = "CALL_PRIORITY";
	String strActivated;

	public static BootUpReceiver newInstance() {
		BootUpReceiver bootUpReceiver = new BootUpReceiver();
		return bootUpReceiver;
	}

	@Override
	public void onReceive(Context context, Intent intent) {

		shPreferences = context.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		strActivated = shPreferences.getString("Activated", "");
		Log.e("strActivated", "" + strActivated);

		if (strActivated.equals("Yes")) {

			Log.e("BootUpReceiver", "BootUpReceiver");
			Intent i = new Intent(context, MainActivityTest.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		}

	}

}
