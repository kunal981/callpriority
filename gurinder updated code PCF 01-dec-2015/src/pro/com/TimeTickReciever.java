package pro.com;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import pro.com.data.GeneralContact;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class TimeTickReciever extends BroadcastReceiver {

	private Context context;
	private ArrayList<GeneralContact> myContactList;

	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;

		Log.i("TimetickReciever", "TimetickReciever Called");

		try {
			if (MainActivityTest.db != null && !MainActivityTest.db.isOpen()) {
				MainActivityTest.db.open();
			}

			myContactList = MainActivityTest.db.fetchGeneralContactList();
			Calendar cl = Calendar.getInstance();
			Date currentDate = cl.getTime();

			if (myContactList != null) {
				for (int i = 0; i < myContactList.size(); i++) {
					if (myContactList.get(i).getEndTime().after(currentDate)) {
						// MainActivityTest.db.deleteGeneralContact(myContactList
						// .get(i).getId(), myContactList.get(i)
						// .getContactId());
						Log.i("TimetickReciever", "Contact Deleted");
					}
				}
			}

			if (MainActivityTest.db != null && MainActivityTest.db.isOpen()) {
				MainActivityTest.db.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
