package pro.com;

import java.util.Calendar;
import java.util.Locale;
import pro.com.data.IOUtils;
import pro.com.wheelpicker.ArrayWheelAdapter;
import pro.com.wheelpicker.NumericWheelAdapter;
import pro.com.wheelpicker.WheelView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class HourPickerActivity extends Activity {

	private Button btnOk, btnCancel;
	private WheelView hours;
	private WheelView mins;
	private WheelView ampm;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.hour_picker);

		btnOk = (Button) findViewById(R.id.btnOk);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				String hoursString = IOUtils.getTimeString(hours
						.getCurrentItem());
				intent.putExtra("hours", hoursString);
				setResult(RESULT_OK, intent);
				finish();
			}
		});

		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		hours = (WheelView) findViewById(R.id.hour);
		NumericWheelAdapter hourAdapter = new NumericWheelAdapter(this, 0, 23);
		hourAdapter.setItemResource(R.layout.wheel_text_item);
		hourAdapter.setItemTextResource(R.id.text);

		hours.setViewAdapter(hourAdapter);

		// set current time
		Calendar calendar = Calendar.getInstance(Locale.US);
		hours.setCurrentItem(calendar.get(Calendar.HOUR));
	}
}
