package pro.com;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

public class splash extends Activity {
	Context context;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		context = this;
		// setting the delay time of the splash screen... and open another
		// activity.
		new Handler().postDelayed(new Runnable() {
			public void run() {

				Intent j = new Intent();
				// Intent j = new Intent(splash.this, MainActivityTest.class);
				j.setClass(context, MainActivityTest.class);
				// j.setClass(getBaseContext(), MainActivityTest.class);
				startActivity(j);
				finish();

			}
		}, 3000);
	}
}