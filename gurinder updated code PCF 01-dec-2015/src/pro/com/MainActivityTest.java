package pro.com;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import pro.com.data.ConstantData;
import pro.com.data.GeneralContact;
import pro.com.data.IOUtils;
import pro.com.data.Mode;
import pro.com.data.MyContact;
import pro.com.data.MyTextView;
import android.R.color;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Contacts.People;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Factory;
import android.view.LayoutInflater.Factory2;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivityTest extends Activity implements Runnable {

	Button b1, b2;
	ListView listView;
	MyListAdapter myListAdapter;
	String id;
	String name, start_time, end_time, duration_time, phone_option, phone_mode;
	String stbuilderToString[], stringTemp[];
	String dataFromDB;
	Context context;
	private TextView txtHeader;
	Drawable line, line1, circle;
	String selectContact;
	private TextView txtMode11, txtMode22, txtMode33, txtMode44;
	private MyTextView txtMode1, txtMode2, txtMode3, txtMode4;
	private ArrayList<Mode> modeList;
	private ArrayList<MyContact> contactList;
	private ArrayList<GeneralContact> contactList1;
	private final String PREFS_NAME = "CALL_PRIORITY";
	private SharedPreferences shPreferences;
	private SharedPreferences.Editor editor;
	private Button editButton;
	// private TextView txtExit1, txtExit2, txtExit3, txtExit4;
	private FrameLayout frm1, frm2, frm3, frm4;
	public static DBHelper db;
	private TextView txtStartTitle, txtEndTitle, txtStart, txtEnd;
	private TextView[] txtDays = new TextView[7];
	private BroadcastReceiver TimeTickReceiver;
	private RelativeLayout relDays;
	private Button btnHelp, btnMenu;
	// int poilsi;
	// String compare;
	private Notification notification;
	private NotificationManager nManager;
	private boolean isStopped = false;
	AudioManager amanager;
	String stringend, strTime, strActivated, stringstart;
	Mode mode;
	// private MyContact myContact;
	String difference;
	String mindifference;
	ToggleButton toggle_notifications, toggle_voicemails;
	MyContact incomingContact = null;
	boolean isAnyModeActive = false;
	int index;
	int top;
	String notificaionOn;
	int i = 0;
	boolean flag = false;
	// boolean userRegistered;

	RelativeLayout layoutlist;

	int hourDiff, minDiff;

	/**
	 * Called with the activity is first created.
	 */
	@SuppressWarnings({ "null", "unused" })
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// AudioManager amanager;

		amanager = (AudioManager) getBaseContext().getSystemService(
				AUDIO_SERVICE);

		// amanager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
		//
		// amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);

		setContentView(R.layout.maintest1);
		shPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		strActivated = shPreferences.getString("Activated", "");
		Log.e("strActivatedOnCreate", "" + strActivated);

		editor = shPreferences.edit();
		editor.putInt("CallRecevier", 2);
		editor.commit();

		if (!ConstantData.isFilteringOn) {
			ConstantData.isFilteringOn = true;
		}
		showNotification();
		// startNotification();
		// cancelNotification();
		try {
			if (db == null) {
				db = new DBHelper(getApplicationContext());
			}
			if (db.isOpen() != true) {
				db.open();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		TimeTickReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				Log.i("TimetickReciever", "TimetickReciever Called");
				// showNotification();

				try {
					if (MainActivityTest.db != null
							&& !MainActivityTest.db.isOpen()) {
						MainActivityTest.db.open();
					}

					ArrayList<GeneralContact> myContactList = MainActivityTest.db
							.fetchGeneralContactList();

					Calendar cl = Calendar.getInstance();
					Date currentDate = cl.getTime();

					if (myContactList != null) {
						for (int i = 0; i < myContactList.size(); i++) {
							if (myContactList.get(i).getEndTime()
									.before(currentDate)) {
								MainActivityTest.db.deleteGeneralContact(
										myContactList.get(i).getId(),
										myContactList.get(i).getContactId());

								myListAdapter.notifyDataSetChanged();
								Log.i("TimetickReciever", "Contact Deleted");
							}
						}
					}

					if (MainActivityTest.db != null
							&& MainActivityTest.db.isOpen()) {
						MainActivityTest.db.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		};

		getApplicationContext().registerReceiver(this.TimeTickReceiver,
				new IntentFilter(Intent.ACTION_TIME_TICK));

		b1 = (Button) findViewById(R.id.addcontact); // for addcontact button
		b2 = (Button) findViewById(R.id.addcontact2);
		btnHelp = (Button) findViewById(R.id.questionmark);
		txtHeader = (TextView) findViewById(R.id.txtHeader);
		// txtExit1 = (TextView) findViewById(R.id.txtExit1);
		// txtExit2 = (TextView) findViewById(R.id.txtExit2);
		// txtExit3 = (TextView) findViewById(R.id.txtExit3);
		// txtExit4 = (TextView) findViewById(R.id.txtExit4);
		frm1 = (FrameLayout) findViewById(R.id.FrameLayout01);
		frm2 = (FrameLayout) findViewById(R.id.FrameLayout02);
		frm3 = (FrameLayout) findViewById(R.id.FrameLayout03);
		frm4 = (FrameLayout) findViewById(R.id.FrameLayout04);
		txtMode11 = (TextView) findViewById(R.id.txtMode1);
		txtMode22 = (TextView) findViewById(R.id.txtMode2);
		txtMode33 = (TextView) findViewById(R.id.txtMode3);
		txtMode44 = (TextView) findViewById(R.id.txtMode4);
		txtMode1 = new MyTextView(context, frm1, txtMode11);
		txtMode2 = new MyTextView(context, frm2, txtMode22);
		txtMode3 = new MyTextView(context, frm3, txtMode33);
		txtMode4 = new MyTextView(context, frm4, txtMode44);
		editButton = (Button) findViewById(R.id.editbutton);
		btnMenu = (Button) findViewById(R.id.menubutton);

		txtStartTitle = (TextView) findViewById(R.id.TextView01);
		txtEndTitle = (TextView) findViewById(R.id.TextView02);
		txtStart = (TextView) findViewById(R.id.txtStart);
		txtEnd = (TextView) findViewById(R.id.txtEnd);
		txtDays[0] = (TextView) findViewById(R.id.textView1);
		txtDays[1] = (TextView) findViewById(R.id.textView2);
		txtDays[2] = (TextView) findViewById(R.id.textView3);
		txtDays[3] = (TextView) findViewById(R.id.textView4);
		txtDays[4] = (TextView) findViewById(R.id.textView5);
		txtDays[5] = (TextView) findViewById(R.id.textView6);
		txtDays[6] = (TextView) findViewById(R.id.textView7);
		relDays = (RelativeLayout) findViewById(R.id.relativeLayout3);
		// toggle_notifications = (ToggleButton)
		// findViewById(R.id.toggle_notifications);
		// toggle_voicemails = (ToggleButton)
		// findViewById(R.id.toggle_voicemails);
		listView = (ListView) findViewById(R.id.list);

		line = getApplicationContext().getResources().getDrawable(
				R.drawable.linebar1);
		line1 = getApplicationContext().getResources().getDrawable(
				R.drawable.linebar1);

		circle = getApplicationContext().getResources().getDrawable(
				R.drawable.circle);

		layoutlist = (RelativeLayout) findViewById(R.id.RelativeLayout06);

		Bundle b = this.getIntent().getExtras();
		if (b != null) {
			selectContact = b.getString("SelectContact");
		}

		setModes();

		// if (txtHeader.getText().toString()
		// .equalsIgnoreCase("General")) {
		// Log.e("isempty", "isempty");
		// layoutlist.setBackground(getResources().getDrawable(
		// R.drawable.general_backgrd_image));
		// } else {
		// Log.e("isNottttttempty", "isnottttttttttttempty");
		// layoutlist.setBackground(getResources().getDrawable(
		// R.drawable.background));
		// }

		Timer t = new Timer();
		// Set the schedule function and rate
		t.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				// Called each time when 1000 milliseconds (1 second) (the
				// period parameter)

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						setModes();
						// restore index and position
						listView.setSelectionFromTop(index, top);

					}
				});

			}

		},
		// Set how long before to start calling the TimerTask (in milliseconds)
				0,
				// Set the amount of time between each execution (in
				// milliseconds)
				1000);

		b1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				Intent i1 = new Intent(MainActivityTest.this,
						Contactlist1.class);
				i1.putExtra("currentMode", txtHeader.getText().toString());

				// Mode

				mode = db.fetchModeByName(txtHeader.getText().toString());

				Log.e("currentModemain", "" + txtHeader.getText().toString());

				if (!mode.getModeName().equalsIgnoreCase("General")) {
					i1.putExtra("start_time", mode.getStartTime());
					i1.putExtra("end_time", mode.getEndTime());
					i1.putExtra("ring_type", mode.getRingerOption());
				} else {
					i1.putExtra("start_time", "");
					i1.putExtra("end_time", "");
					i1.putExtra("ring_type", "Vibrate");
				}
				startActivityForResult(i1, 6);

			}
		});

		b2.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				Intent i1 = new Intent(MainActivityTest.this,
						Contactlist1.class);
				i1.putExtra("currentMode", txtHeader.getText());
				Mode mode = db.fetchModeByName(txtHeader.getText().toString());
				i1.putExtra("start_time", mode.getStartTime());
				i1.putExtra("end_time", mode.getEndTime());
				startActivityForResult(i1, 6);

			}
		});
		btnMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Log.e("menuuuuu", "menuuui");
				// TODO Auto-generated method stub
				// btnMenu.setBackgroundResource(R.drawable.menu_button);
				// if (!userRegistered) {
				// userRegistered = true;
				openMenu();
				// }

				// Log.e("userRegistered", "" + userRegistered);

				// boolean userRegisteredTemp = userRegistered;

				// closeOptionsMenu();

				// if (userRegistered) {
				// userRegistered = false;
				// btnMenu.setBackground(getResources().getDrawable(
				// R.drawable.btn));
				// Log.e("iffff", "ifff");
				// // closeOptionsMenu();
				// } else {
				// userRegistered = true;
				//
				// Log.e("elsee", "elsee");
				// btnMenu.setBackground(getResources().getDrawable(
				// R.drawable.btn_slct));
				// // openOptionsMenu();
				// }
				//
				//
				// if (userRegisteredTemp) {
				// Log.e("iffff", "ifff");
				// closeOptionsMenu();
				// } else {
				// Log.e("elsee", "elsee");
				// openOptionsMenu();
				// }

			}
		});

		// amanager = (AudioManager) getBaseContext().getSystemService(
		// AUDIO_SERVICE);
		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
		strTime = sdf.format(c.getTime());

		// mode = db.fetchModeByName(txtHeader.getText().toString());
		//
		// Log.e("mode", "" + mode.getRingerOption());
		// try {
		// if (mode.getRingerOption().equalsIgnoreCase("Vibrate")) {
		//
		// Log.e("MainVibrate", "" + mode);
		// amanager = (AudioManager) getBaseContext().getSystemService(
		// AUDIO_SERVICE);
		// amanager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
		// amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
		// Log.e("here", "here");
		// // amanager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
		//
		// } else if (mode.getRingerOption().equalsIgnoreCase("Ring"))
		//
		// {
		// Log.e("Mainnormal", "" + mode);
		//
		// amanager = null;
		//
		// Log.e("else", "else");
		// amanager = (AudioManager) getBaseContext().getSystemService(
		// AUDIO_SERVICE);
		// amanager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
		// amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
		// }
		//
		// } catch (Exception e) {
		//
		// }

		btnHelp.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent intent = new Intent(MainActivityTest.this,
						HelpActivity.class);
				startActivity(intent);

			}
		});

		// notificaionOn = shPreferences.getString("Notification", "");
		// Log.e("notificaionOn", ""+notificaionOn);
		// if(notificaionOn.equals("Off")){
		// toggle_notifications.setChecked(false);
		// }else{
		// toggle_notifications.setChecked(true);
		// }
		//
		// toggle_notifications
		// .setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		// @Override
		// public void onCheckedChanged(CompoundButton buttonView,
		// boolean isChecked) {
		// // TODO Auto-generated method stub
		//
		// if(isChecked){
		//
		// toggle_notifications.setChecked(true);
		//
		//
		// editor = shPreferences.edit();
		// editor.putString("Notification", "On");
		// editor.commit();
		// Log.e("shrdsaved", "On");
		//
		// amanager = (AudioManager) getBaseContext()
		// .getSystemService(AUDIO_SERVICE);
		//
		// amanager.setStreamMute(AudioManager.STREAM_SYSTEM,
		// true);
		//
		// amanager.setStreamMute(
		// AudioManager.STREAM_NOTIFICATION, true);
		//
		// Toast.makeText(getApplicationContext(),
		// "Notifications Sounds are off", 1000)
		// .show();
		// i++;
		// editor.putInt("count", i);
		// editor.commit();
		// Log.e("i", ""+i);
		// }else{
		//
		// toggle_notifications.setChecked(false);
		//
		// editor = shPreferences.edit();
		// editor.putString("Notification", "Off");
		// editor.commit();
		// Log.e("shrdsaved", "Off");
		//
		// // Log.e("else", "else");
		// amanager = (AudioManager) getBaseContext()
		// .getSystemService(AUDIO_SERVICE);
		//
		// amanager.setStreamMute(AudioManager.STREAM_SYSTEM,
		// false);
		//
		// amanager.setStreamMute(
		// AudioManager.STREAM_NOTIFICATION, false);
		// Toast.makeText(getApplicationContext(),
		// "Notifications Sounds are On", 1000).show();
		// }
		//
		// }
		// });

		editButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// if (flag) {
				// flag = false;
				// editButton.setBackground(getResources().getDrawable(
				// R.drawable.btn));
				// } else {
				// flag = true;
				//
				// editButton.setBackground(getResources().getDrawable(
				// R.drawable.btn_slct));
				// }
				//
				Mode mode1 = null;
				Mode mode2 = null;
				Mode mode3 = null;
				Mode mode4 = null;

				for (int i = 0; i < modeList.size(); i++) {

					switch (modeList.get(i).getModeNumber()) {
					case 1:
						mode1 = modeList.get(i);
					case 2:
						mode2 = modeList.get(i);
					case 3:
						mode3 = modeList.get(i);
					case 4:
						mode4 = modeList.get(i);
						break;

					default:
						break;
					}
				}

				if (txtMode1.getTag() != null
						&& txtMode1.getTag().toString().equalsIgnoreCase("ON")) {
					ConstantData.mode = mode1;
					Intent _startmode = new Intent(MainActivityTest.this,
							ModeSetting11.class);
					_startmode.putExtra("MODE_NO", 1);
					startActivityForResult(_startmode, 1);
				} else if (txtMode2.getTag() != null
						&& txtMode2.getTag().toString().equalsIgnoreCase("ON")) {
					ConstantData.mode = mode2;
					Intent _startmode = new Intent(MainActivityTest.this,
							ModeSetting11.class);
					_startmode.putExtra("MODE_NO", 2);
					startActivityForResult(_startmode, 2);
				} else if (txtMode3.getTag() != null
						&& txtMode3.getTag().toString().equalsIgnoreCase("ON")) {
					ConstantData.mode = mode3;
					Intent _startmode = new Intent(MainActivityTest.this,
							ModeSetting11.class);
					_startmode.putExtra("MODE_NO", 3);
					startActivityForResult(_startmode, 3);
				} else if (txtMode4.getTag() != null
						&& txtMode4.getTag().toString().equalsIgnoreCase("ON")) {
					ConstantData.mode = mode4;
					Intent _startmode = new Intent(MainActivityTest.this,
							ModeSetting11.class);
					_startmode.putExtra("MODE_NO", 4);
					startActivityForResult(_startmode, 4);
				}
			}
		});

		frm1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (flag) {
					flag = false;
					// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
					frm1.setBackgroundResource(R.drawable.menu1);
				} else {
					flag = true;

					// frm1.setBackground(getResources().getDrawable(
					// R.drawable.slct));

					frm1.setBackgroundResource(R.drawable.slct_new);
					frm2.setBackgroundResource(R.drawable.menu2);
					frm3.setBackgroundResource(R.drawable.menu3);
					frm4.setBackgroundResource(R.drawable.menu4);

					// frm1.setBackground(getResources().getDrawable(
					// R.drawable.slct_new));
					// frm2.setBackground(getResources().getDrawable(
					// R.drawable.menu2));
					// frm3.setBackground(getResources().getDrawable(
					// R.drawable.menu3));
					// frm4.setBackground(getResources().getDrawable(
					// R.drawable.menu4));
				}

				if (txtMode1.getTag() == null) {
					Intent _startmode = new Intent(MainActivityTest.this,
							ModeSetting11.class);
					_startmode.putExtra("MODE_NO", 1);
					startActivityForResult(_startmode, 1);

				} else if (txtMode1.getTag().toString().equalsIgnoreCase("OFF")) {
					setMode1();
				} else if (txtMode1.getTag().toString().equalsIgnoreCase("ON")) {

					Mode mode = null;

					for (int i = 0; i < modeList.size(); i++) {
						if (modeList.get(i).getModeNumber() == 1) {
							mode = modeList.get(i);
						}
					}

					txtMode1.setTag("OFF");
					txtMode1.setText(mode.getModeName());
					// txtExit1.setVisibility(View.INVISIBLE);
					txtHeader.setText("General");

					txtStartTitle.setVisibility(View.INVISIBLE);
					txtEndTitle.setVisibility(View.INVISIBLE);
					txtStart.setVisibility(View.INVISIBLE);
					txtEnd.setVisibility(View.INVISIBLE);
					relDays.setVisibility(View.INVISIBLE);

					frm1.setBackgroundResource(R.drawable.menu1);
					frm2.setBackgroundResource(R.drawable.menu2);
					frm3.setBackgroundResource(R.drawable.menu3);
					frm4.setBackgroundResource(R.drawable.menu4);

					// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
					// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
					// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
					// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

					setContacts();
					editor = shPreferences.edit();
					editor.putInt("MODE_NO", 0);
					editor.commit();
				}

			}
		});
		frm2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (flag) {
					flag = false;
					frm2.setBackgroundResource(R.drawable.menu2);
					// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
				} else {
					flag = true;

					// frm2.setBackground(getResources().getDrawable(
					// R.drawable.slct2));
					//
					frm2.setBackgroundResource(R.drawable.slct_new2);
					frm1.setBackgroundResource(R.drawable.menu1);
					frm3.setBackgroundResource(R.drawable.menu3);
					frm4.setBackgroundResource(R.drawable.menu4);

					// frm2.setBackground(getResources().getDrawable(R.drawable.slct_new2));
					// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
					// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
					// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

				}

				if (txtMode2.getTag() == null) {
					Intent _startmode = new Intent(MainActivityTest.this,
							ModeSetting11.class);
					_startmode.putExtra("MODE_NO", 2);
					startActivityForResult(_startmode, 2);

				} else if (txtMode2.getTag().toString().equalsIgnoreCase("OFF")) {
					setMode2();
				} else if (txtMode2.getTag().toString().equalsIgnoreCase("ON")) {

					Mode mode = null;

					for (int i = 0; i < modeList.size(); i++) {
						if (modeList.get(i).getModeNumber() == 2) {
							mode = modeList.get(i);
						}
					}

					txtMode2.setTag("OFF");
					txtMode2.setText(mode.getModeName());

					// txtExit2.setVisibility(View.INVISIBLE);
					txtHeader.setText("General");

					txtStartTitle.setVisibility(View.INVISIBLE);
					txtEndTitle.setVisibility(View.INVISIBLE);
					txtStart.setVisibility(View.INVISIBLE);
					txtEnd.setVisibility(View.INVISIBLE);
					relDays.setVisibility(View.INVISIBLE);

					frm1.setBackgroundResource(R.drawable.menu1);
					frm2.setBackgroundResource(R.drawable.menu2);
					frm3.setBackgroundResource(R.drawable.menu3);
					frm4.setBackgroundResource(R.drawable.menu4);

					// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
					// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
					// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
					// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

					editor = shPreferences.edit();
					editor.putInt("MODE_NO", 0);
					editor.commit();
					setContacts();
				}
			}
		});
		frm3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (flag) {
					flag = false;
					frm3.setBackgroundResource(R.drawable.menu3);

					// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
				} else {
					flag = true;

					// frm3.setBackground(getResources().getDrawable(
					// R.drawable.slct3));

					frm3.setBackgroundResource(R.drawable.slct_new3);
					frm1.setBackgroundResource(R.drawable.menu1);
					frm2.setBackgroundResource(R.drawable.menu2);
					frm4.setBackgroundResource(R.drawable.menu4);

					// frm3.setBackground(getResources().getDrawable(R.drawable.slct_new3));
					// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
					// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
					// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));
				}

				if (txtMode3.getTag() == null) {
					Intent _startmode = new Intent(MainActivityTest.this,
							ModeSetting11.class);
					_startmode.putExtra("MODE_NO", 3);
					startActivityForResult(_startmode, 3);

				} else if (txtMode3.getTag().toString().equalsIgnoreCase("OFF")) {
					setMode3();
				} else if (txtMode3.getTag().toString().equalsIgnoreCase("ON")) {

					Mode mode = null;

					for (int i = 0; i < modeList.size(); i++) {
						if (modeList.get(i).getModeNumber() == 3) {
							mode = modeList.get(i);
						}
					}

					txtMode3.setTag("OFF");
					txtMode3.setText(mode.getModeName());
					// txtExit3.setVisibility(View.INVISIBLE);
					txtHeader.setText("General");
					setContacts();

					txtStartTitle.setVisibility(View.INVISIBLE);
					txtEndTitle.setVisibility(View.INVISIBLE);
					txtStart.setVisibility(View.INVISIBLE);
					txtEnd.setVisibility(View.INVISIBLE);
					relDays.setVisibility(View.INVISIBLE);

					frm1.setBackgroundResource(R.drawable.menu1);
					frm2.setBackgroundResource(R.drawable.menu2);
					frm3.setBackgroundResource(R.drawable.menu3);
					frm4.setBackgroundResource(R.drawable.menu4);

					// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
					// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
					// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
					// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

					editor = shPreferences.edit();
					editor.putInt("MODE_NO", 0);
					editor.commit();
				}

			}
		});
		frm4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (flag) {
					flag = false;
					frm4.setBackgroundResource(R.drawable.menu4);

					// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

				} else {
					flag = true;

					// frm4.setBackground(getResources().getDrawable(
					// R.drawable.slct4));

					frm4.setBackgroundResource(R.drawable.slct_new4);
					frm1.setBackgroundResource(R.drawable.menu1);
					frm2.setBackgroundResource(R.drawable.menu2);
					frm3.setBackgroundResource(R.drawable.menu3);

					// frm4.setBackground(getResources().getDrawable(R.drawable.slct_new4));
					// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
					// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
					// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
				}

				if (txtMode4.getTag() == null) {
					Intent _startmode = new Intent(MainActivityTest.this,
							ModeSetting11.class);
					_startmode.putExtra("MODE_NO", 4);
					startActivityForResult(_startmode, 4);

				} else if (txtMode4.getTag().toString().equalsIgnoreCase("OFF")) {
					setMode4();
				} else if (txtMode4.getTag().toString().equalsIgnoreCase("ON")) {

					Mode mode = null;

					for (int i = 0; i < modeList.size(); i++) {
						if (modeList.get(i).getModeNumber() == 4) {
							mode = modeList.get(i);
						}
					}

					txtMode4.setTag("OFF");
					txtMode4.setText(mode.getModeName());
					// txtExit4.setVisibility(View.INVISIBLE);
					txtHeader.setText("General");
					setContacts();

					txtStartTitle.setVisibility(View.INVISIBLE);
					txtEndTitle.setVisibility(View.INVISIBLE);
					txtStart.setVisibility(View.INVISIBLE);
					txtEnd.setVisibility(View.INVISIBLE);
					relDays.setVisibility(View.INVISIBLE);

					frm1.setBackgroundResource(R.drawable.menu1);
					frm2.setBackgroundResource(R.drawable.menu2);
					frm3.setBackgroundResource(R.drawable.menu3);
					frm4.setBackgroundResource(R.drawable.menu4);

					// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
					// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
					// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
					// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

					editor = shPreferences.edit();
					editor.putInt("MODE_NO", 0);
					editor.commit();
				}

			}
		});

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				ConstantData.myContact = contactList.get(position);
				Intent intent = new Intent(getApplicationContext(),
						ContactSetting.class);
				// intent.putExtra("currentMode", (String) txtHeader.getText());
				intent.putExtra("currentMode", txtHeader.getText().toString());

				startActivityForResult(intent, 5);

				// Log.e("gid:",""+contactList.get(position));

				Log.e("groupid:", contactList.get(position).getGroupId());
				Log.e("phn:", contactList.get(position).getPhoneNumber());
				Log.e("position:", "" + position);
				// if (contactList.get(position).getGroupId().equals("-1")) {
				//
				// Intent intent = new Intent(getApplicationContext(),
				// ContactSetting.class);
				// intent.putExtra("currentMode", (String) txtHeader.getText());
				// startActivityForResult(intent, 5);
				//
				// } else {
				// Log.e("else:", "else");
				//
				// Intent intent = new Intent(getApplicationContext(),
				// ContactSetting.class);
				// intent.putExtra("currentMode", (String) txtHeader.getText());
				// startActivity(intent);
				// // startActivityForResult(intent, 5);
				// }

			}
		});

		listView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				index = listView.getFirstVisiblePosition();
				View v = listView.getChildAt(0);
				top = (v == null) ? 0 : (v.getTop() - listView.getPaddingTop());

				// Log.e("top", "" + top);
			}
		});

		// Calendar c = Calendar.getInstance();
		// SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
		// strTime = sdf.format(c.getTime());
		//
		// Log.e("systemtime", strTime);

	}

	// @Override
	// protected void onResume() {
	// // TODO Auto-generated method stub
	// super.onResume();
	// Log.e("oncreateeeeeeeeeeeeeeee", "oncreate");
	// // btnMenu.setBackground(getResources().getDrawable(R.drawable.btn));
	// }

	private void setMode0() {
		try {

			Mode mode1 = null;
			Mode mode2 = null;
			Mode mode3 = null;
			Mode mode4 = null;

			for (int i = 0; i < modeList.size(); i++) {

				switch (modeList.get(i).getModeNumber()) {
				case 1:
					mode1 = modeList.get(i);
					break;
				case 2:
					mode2 = modeList.get(i);
					break;
				case 3:
					mode3 = modeList.get(i);
					break;
				case 4:
					mode4 = modeList.get(i);
					break;
				default:
					break;
				}
			}

			txtHeader.setText(modeList.get(0).getModeName());

			setContactsByMode(modeList.get(0).getModeName());

			editor = shPreferences.edit();
			editor.putInt("MODE_NO", 0);
			editor.commit();

			txtStartTitle.setVisibility(View.INVISIBLE);
			txtEndTitle.setVisibility(View.INVISIBLE);
			txtStart.setVisibility(View.INVISIBLE);
			txtEnd.setVisibility(View.INVISIBLE);
			relDays.setVisibility(View.INVISIBLE);

			frm1.setBackgroundResource(R.drawable.menu1);
			frm2.setBackgroundResource(R.drawable.menu2);
			frm3.setBackgroundResource(R.drawable.menu3);
			frm4.setBackgroundResource(R.drawable.menu4);

			// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
			// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
			// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
			// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

			if (txtMode1.getTag() != null)
				txtMode1.setTag("OFF");
			if (txtMode2.getTag() != null)
				txtMode2.setTag("OFF");
			if (txtMode3.getTag() != null)
				txtMode3.setTag("OFF");
			if (txtMode4.getTag() != null)
				txtMode4.setTag("OFF");
			try {
				txtMode1.setText(mode1.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit1.setVisibility(View.INVISIBLE);
			try {
				txtMode2.setText(mode2.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit2.setVisibility(View.INVISIBLE);
			try {
				txtMode3.setText(mode3.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit3.setVisibility(View.INVISIBLE);
			try {
				txtMode4.setText(mode4.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit4.setVisibility(View.INVISIBLE);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setMode1() {
		try {

			Mode mode1 = null;
			Mode mode2 = null;
			Mode mode3 = null;
			Mode mode4 = null;

			for (int i = 0; i < modeList.size(); i++) {

				switch (modeList.get(i).getModeNumber()) {
				case 1:
					mode1 = modeList.get(i);
					break;
				case 2:
					mode2 = modeList.get(i);
					break;
				case 3:
					mode3 = modeList.get(i);
					break;
				case 4:
					mode4 = modeList.get(i);
					break;

				default:
					break;
				}
			}

			txtHeader.setText(mode1.getModeName());
			txtMode1.setText(mode1.getModeName());

			// txtExit1.setVisibility(View.VISIBLE);
			txtMode1.setTag("ON");
			setContactsByMode(mode1.getModeName());

			editor = shPreferences.edit();
			editor.putInt("MODE_NO", 1);
			editor.commit();

			txtStartTitle.setVisibility(View.VISIBLE);
			txtEndTitle.setVisibility(View.VISIBLE);
			txtStart.setVisibility(View.VISIBLE);
			txtEnd.setVisibility(View.VISIBLE);
			relDays.setVisibility(View.VISIBLE);

			// frm1.setBackground(getResources().getDrawable(R.drawable.slct));

			frm1.setBackgroundResource(R.drawable.slct_new);
			frm2.setBackgroundResource(R.drawable.menu2);
			frm3.setBackgroundResource(R.drawable.menu3);
			frm4.setBackgroundResource(R.drawable.menu4);

			// frm1.setBackground(getResources().getDrawable(R.drawable.slct_new));
			// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
			// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
			// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

			// txtMode1.setTextColor(Color.parseColor("#FFFFFF"));

			txtStart.setText(mode1.getStartTime()
					.substring(0, mode1.getStartTime().length())
					.replaceAll(" ", "").toLowerCase());
			txtEnd.setText(mode1.getEndTime()
					.substring(0, mode1.getEndTime().length())
					.replaceAll(" ", "").toLowerCase());
			setDays(mode1.getDay());

			if (txtMode2.getTag() != null)
				txtMode2.setTag("OFF");
			if (txtMode3.getTag() != null)
				txtMode3.setTag("OFF");
			if (txtMode4.getTag() != null)
				txtMode4.setTag("OFF");
			try {
				txtMode2.setText(mode2.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit2.setVisibility(View.INVISIBLE);
			try {
				txtMode3.setText(mode3.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit3.setVisibility(View.INVISIBLE);
			try {
				txtMode4.setText(mode4.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit4.setVisibility(View.INVISIBLE);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setMode2() {
		try {

			Mode mode1 = null;
			Mode mode2 = null;
			Mode mode3 = null;
			Mode mode4 = null;

			for (int i = 0; i < modeList.size(); i++) {

				switch (modeList.get(i).getModeNumber()) {
				case 1:
					mode1 = modeList.get(i);
					break;
				case 2:
					mode2 = modeList.get(i);
					break;
				case 3:
					mode3 = modeList.get(i);
					break;
				case 4:
					mode4 = modeList.get(i);
					break;

				default:
					break;
				}
			}

			txtHeader.setText(mode2.getModeName());
			txtMode2.setText(mode2.getModeName());
			// txtExit2.setVisibility(View.VISIBLE);
			txtMode2.setTag("ON");
			setContactsByMode(mode2.getModeName());

			editor = shPreferences.edit();
			editor.putInt("MODE_NO", 2);
			editor.commit();

			txtStartTitle.setVisibility(View.VISIBLE);
			txtEndTitle.setVisibility(View.VISIBLE);
			txtStart.setVisibility(View.VISIBLE);
			txtEnd.setVisibility(View.VISIBLE);
			relDays.setVisibility(View.VISIBLE);

			frm1.setBackgroundResource(R.drawable.menu1);
			frm2.setBackgroundResource(R.drawable.slct_new2);
			frm3.setBackgroundResource(R.drawable.menu3);
			frm4.setBackgroundResource(R.drawable.menu4);

			// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
			// frm2.setBackground(getResources().getDrawable(R.drawable.slct_new2));
			// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
			// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

			txtStart.setText(mode2.getStartTime()
					.substring(0, mode2.getStartTime().length())
					.replaceAll(" ", "").toLowerCase());
			txtEnd.setText(mode2.getEndTime()
					.substring(0, mode2.getEndTime().length())
					.replaceAll(" ", "").toLowerCase());
			setDays(mode2.getDay());
			;

			if (txtMode1.getTag() != null)
				txtMode1.setTag("OFF");
			if (txtMode3.getTag() != null)
				txtMode3.setTag("OFF");
			if (txtMode4.getTag() != null)
				txtMode4.setTag("OFF");

			try {
				txtMode1.setText(mode1.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit1.setVisibility(View.INVISIBLE);
			try {
				txtMode3.setText(mode3.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit3.setVisibility(View.INVISIBLE);
			try {
				txtMode4.setText(mode4.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit4.setVisibility(View.INVISIBLE);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setMode3() {
		try {

			Mode mode1 = null;
			Mode mode2 = null;
			Mode mode3 = null;
			Mode mode4 = null;

			for (int i = 0; i < modeList.size(); i++) {

				switch (modeList.get(i).getModeNumber()) {
				case 1:
					mode1 = modeList.get(i);
					break;
				case 2:
					mode2 = modeList.get(i);
					break;
				case 3:
					mode3 = modeList.get(i);
					break;
				case 4:
					mode4 = modeList.get(i);
					break;

				default:
					break;
				}
			}

			txtHeader.setText(mode3.getModeName());
			txtMode3.setText(mode3.getModeName());
			// txtExit3.setVisibility(View.VISIBLE);
			txtMode3.setTag("ON");
			setContactsByMode(mode3.getModeName());

			editor = shPreferences.edit();
			editor.putInt("MODE_NO", 3);
			editor.commit();

			txtStartTitle.setVisibility(View.VISIBLE);
			txtEndTitle.setVisibility(View.VISIBLE);
			txtStart.setVisibility(View.VISIBLE);
			txtEnd.setVisibility(View.VISIBLE);
			relDays.setVisibility(View.VISIBLE);

			frm1.setBackgroundResource(R.drawable.menu1);
			frm2.setBackgroundResource(R.drawable.menu2);
			frm3.setBackgroundResource(R.drawable.slct_new3);
			frm4.setBackgroundResource(R.drawable.menu4);

			// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
			// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
			// frm3.setBackground(getResources().getDrawable(R.drawable.slct_new3));
			// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

			txtStart.setText(mode3.getStartTime()
					.substring(0, mode3.getStartTime().length())
					.replaceAll(" ", "").toLowerCase());
			txtEnd.setText(mode3.getEndTime()
					.substring(0, mode3.getEndTime().length())
					.replaceAll(" ", "").toLowerCase());
			setDays(mode3.getDay());
			;

			if (txtMode1.getTag() != null)
				txtMode1.setTag("OFF");
			if (txtMode2.getTag() != null)
				txtMode2.setTag("OFF");
			if (txtMode4.getTag() != null)
				txtMode4.setTag("OFF");
			try {
				txtMode1.setText(mode1.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit1.setVisibility(View.INVISIBLE);
			try {
				txtMode2.setText(mode2.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit2.setVisibility(View.INVISIBLE);
			try {
				txtMode4.setText(mode4.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit4.setVisibility(View.INVISIBLE);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setMode4() {
		try {

			Mode mode1 = null;
			Mode mode2 = null;
			Mode mode3 = null;
			Mode mode4 = null;

			for (int i = 0; i < modeList.size(); i++) {

				switch (modeList.get(i).getModeNumber()) {
				case 1:
					mode1 = modeList.get(i);
					break;
				case 2:
					mode2 = modeList.get(i);
					break;
				case 3:
					mode3 = modeList.get(i);
					break;
				case 4:
					mode4 = modeList.get(i);
					break;

				default:
					break;
				}
			}

			txtHeader.setText(mode4.getModeName());
			txtMode4.setText(mode4.getModeName());
			// txtExit4.setVisibility(View.VISIBLE);
			txtMode4.setTag("ON");
			setContactsByMode(mode4.getModeName());

			editor = shPreferences.edit();
			editor.putInt("MODE_NO", 4);
			editor.commit();

			txtStartTitle.setVisibility(View.VISIBLE);
			txtEndTitle.setVisibility(View.VISIBLE);
			txtStart.setVisibility(View.VISIBLE);
			txtEnd.setVisibility(View.VISIBLE);
			relDays.setVisibility(View.VISIBLE);

			frm1.setBackgroundResource(R.drawable.menu1);
			frm2.setBackgroundResource(R.drawable.menu2);
			frm3.setBackgroundResource(R.drawable.menu3);
			// frm4.setBackground(getResources().getDrawable(R.drawable.slct4));
			frm4.setBackgroundResource(R.drawable.slct_new4);

			// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
			// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
			// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
			// //
			// frm4.setBackground(getResources().getDrawable(R.drawable.slct4));
			// frm4.setBackground(getResources().getDrawable(R.drawable.slct_new4));

			txtStart.setText(mode4.getStartTime()
					.substring(0, mode4.getStartTime().length())
					.replaceAll(" ", "").toLowerCase());
			txtEnd.setText(mode4.getEndTime()
					.substring(0, mode4.getEndTime().length())
					.replaceAll(" ", "").toLowerCase());
			setDays(mode4.getDay());
			;

			if (txtMode1.getTag() != null)
				txtMode1.setTag("OFF");
			if (txtMode3.getTag() != null)
				txtMode3.setTag("OFF");
			if (txtMode2.getTag() != null)
				txtMode2.setTag("OFF");
			try {
				txtMode1.setText(mode1.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit1.setVisibility(View.INVISIBLE);
			try {
				txtMode2.setText(mode2.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit2.setVisibility(View.INVISIBLE);
			try {
				txtMode3.setText(mode3.getModeName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// txtExit3.setVisibility(View.INVISIBLE);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int reqCode, int resCode, Intent data) {
		super.onActivityResult(reqCode, resCode, data);

		if (resCode == Activity.RESULT_OK) {

			modeList = db.fetchModeListByOrder();

			if (reqCode == 1) {
				setModes();
				boolean isDelete = data.getExtras().getBoolean("isDelete");
				if (isDelete) {
					setMode0();
					txtMode1.setText("");
					// txtExit1.setVisibility(View.INVISIBLE);
					modeList = db.fetchModeListByOrder();
					finish();
				} else {
					setMode1();
				}
				b1.setEnabled(true);
			} else if (reqCode == 2) {
				setModes();
				boolean isDelete = data.getExtras().getBoolean("isDelete");
				if (isDelete) {
					setMode0();
					txtMode2.setText("");
					// txtExit2.setVisibility(View.INVISIBLE);
					modeList = db.fetchModeListByOrder();
					finish();
				} else {
					setMode2();
				}
				b1.setEnabled(true);
			} else if (reqCode == 3) {
				setModes();
				boolean isDelete = data.getExtras().getBoolean("isDelete");
				if (isDelete) {
					setMode0();
					txtMode3.setText("");
					// txtExit3.setVisibility(View.INVISIBLE);
					modeList = db.fetchModeListByOrder();
					finish();
				} else {
					setMode3();
				}
				b1.setEnabled(true);
			} else if (reqCode == 4) {
				setModes();
				boolean isDelete = data.getExtras().getBoolean("isDelete");
				if (isDelete) {
					setMode0();
					txtMode4.setText("");
					// txtExit4.setVisibility(View.INVISIBLE);
					modeList = db.fetchModeListByOrder();
					finish();
				} else {
					setMode4();
				}
				b1.setEnabled(true);
			} else if (reqCode == 6) {
				// String mode = (String) txtHeader.getText();
				String mode = txtHeader.getText().toString();
				try {
					mode = data.getExtras().getString("MODE");
				} catch (Exception e) {
					e.printStackTrace();
				}

				for (int i = 0; i < modeList.size(); i++) {
					if (modeList.get(i).getModeName().equalsIgnoreCase(mode)) {
						switch (modeList.get(i).getModeNumber()) {
						case 0:
							setMode0();
							break;
						case 1:
							setMode1();
							break;
						case 2:
							setMode2();
							break;
						case 3:
							setMode3();
							break;
						case 4:
							setMode4();
							break;
						default:
							break;
						}
					}
				}
			} else if (reqCode == 5) {
				String mode = data.getExtras().getString("MODE");
				for (int i = 0; i < modeList.size(); i++) {
					if (modeList.get(i).getModeName().equalsIgnoreCase(mode)) {
						switch (modeList.get(i).getModeNumber()) {
						case 0:
							setMode0();
							break;
						case 1:
							setMode1();
							break;
						case 2:
							setMode2();
							break;
						case 3:
							setMode3();
							break;
						case 4:
							setMode4();
							break;
						default:
							break;
						}
					}
				}
			} else {
				Uri contactData = data.getData();
				Cursor c = managedQuery(contactData, null, null, null, null);
				c.moveToFirst();
				String name;
				name = c.getString(c.getColumnIndexOrThrow(People.NAME));
				TextView tv;
			}
		}
	}

	// @Override
	// public boolean onPrepareOptionsMenu(Menu menu) {
	//
	// menu.clear();
	//
	// if (ConstantData.isFilteringOn) {
	// menu.add(0, 1, 1, "Shutdown Call Filtering");
	// }
	//
	// menu.add(0, 2, 2, "Minimize");
	// menu.add(0, 3, 3, "Cancel notification");
	// menu.add(0, 4,4, "Automatically open call Filtering");
	//
	// return super.onPrepareOptionsMenu(menu);
	// }

	// public boolean onOptionsItemSelected(MenuItem item) {
	// // TODO Auto-generated method stub
	// super.onOptionsItemSelected(item);
	//
	// switch (item.getItemId()) {
	// case 1:
	// exitOptionsDialog();
	//
	// break;
	// case 2:
	// moveTaskToBack(true);
	// break;
	// case 3:
	// cancelNotification();
	// break;
	// }
	// return true;
	// }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		{
			super.onCreateOptionsMenu(menu);
			MenuInflater inflater = getMenuInflater();

			if (strActivated.equals("Yes")) {
				menu.add(0, 4, 4, "Do Not Auto Start PCF");
			} else {
				menu.add(0, 4, 4, "Auto Restart PCF ");
			}
			inflater.inflate(R.menu.menu, menu);

			// setMenuBackground();

			return true;
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		super.onOptionsItemSelected(item);

		switch (item.getItemId()) {

		case R.id.shutdown:
			exitOptionsDialog();

			break;
		case R.id.minimize:
			moveTaskToBack(true);
			// btnMenu.setBackground(getResources().getDrawable(R.drawable.btn));
			btnMenu.setBackgroundResource(R.drawable.btn);
			break;
		case R.id.cancel_notification:
			cancelNotification();
			btnMenu.setBackgroundResource(R.drawable.btn);
			// btnMenu.setBackground(getResources().getDrawable(R.drawable.btn));
			break;
		// case R.id.start_app:
		case 4:
			strActivated = shPreferences.getString("Activated", "");
			btnMenu.setBackgroundResource(R.drawable.btn);
			// btnMenu.setBackground(getResources().getDrawable(R.drawable.btn));
			// Log.e("strActivated", "" + strActivated);
			if (strActivated.equals("Yes")) {

				item.setTitle("Auto Restart PCF");

				editor = shPreferences.edit();
				editor.putString("Activated", "No");
				editor.commit();
				// Log.e("strActivatedDeactive", "" + strActivated);

				PackageManager pm = MainActivityTest.this.getPackageManager();
				ComponentName componentName = new ComponentName(
						MainActivityTest.this, BootUpReceiver.class);
				pm.setComponentEnabledSetting(componentName,
						PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
						PackageManager.DONT_KILL_APP);
				Toast.makeText(getApplicationContext(),
						"PCF will Not Start when Phone Restarts",
						Toast.LENGTH_LONG).show();

				// startOptionsDialog();
			} else {
				item.setTitle("Do Not Auto Start PCF");
				editor = shPreferences.edit();
				editor.putString("Activated", "Yes");
				editor.commit();

				// Log.e("strActivatedActivte", "" + strActivated);
				PackageManager pm = MainActivityTest.this.getPackageManager();
				ComponentName componentName = new ComponentName(
						MainActivityTest.this, BootUpReceiver.class);
				pm.setComponentEnabledSetting(componentName,
						PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
						PackageManager.DONT_KILL_APP);
				Toast.makeText(getApplicationContext(),
						"PCF will Start when Phone Restarts", Toast.LENGTH_LONG)
						.show();
			}

			break;

		}
		return true;
	}

	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem register = menu.findItem(R.id.shutdown);

		return true;
	}

	// @Override
	// public boolean onPrepareOptionsMenu(Menu menu) {
	// // Close the menu after a period of time.
	// // Note that this STARTS the timer when the options menu is being
	// // prepared, NOT when the menu is made visible.
	// Timer timing = new Timer();
	// timing.schedule(new TimerTask() {
	//
	// /**
	// * {@inheritDoc}
	// */
	// @Override
	// public void run() {
	// closeOptionsMenu();
	// }
	// }, 1000);
	// return super.onPrepareOptionsMenu(menu);
	// }

	// public void sharedprefrence() {
	// editor = shPreferences.edit();
	//
	// editor.putString("Activated", "Yes");
	// editor.commit();
	//
	// }

	private void exitOptionsDialog() {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage("Do you want to shutdown Priority Call Filter?")
				.setNegativeButton(R.string.str_no,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
							}
						})
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								// ConstantData.isFilteringOn = false;

								cancelNotification();

								amanager = null;

								amanager = (AudioManager) getBaseContext()
										.getSystemService(AUDIO_SERVICE);

								// Log.e("N SoundOn", "N SoundOn");
								// Log.e("amanager", "" + amanager.toString());

								// int maxVolume = amanager
								// .getStreamMaxVolume(AudioManager.STREAM_RING);
								//
								// amanager.setStreamVolume(
								// AudioManager.STREAM_NOTIFICATION,maxVolume,
								// AudioManager.FLAG_SHOW_UI
								// + AudioManager.FLAG_PLAY_SOUND);

								amanager.setStreamSolo(
										AudioManager.STREAM_SYSTEM, false);
								amanager.setStreamSolo(
										AudioManager.STREAM_NOTIFICATION, false);

								Intent intent = new Intent(Intent.ACTION_MAIN);
								intent.addCategory(Intent.CATEGORY_HOME);
								intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);

								editor = shPreferences.edit();
								editor.putInt("CallRecevier", 1);
								editor.commit();

								finish();
								moveTaskToBack(true);
								System.exit(0);
								// amanager.setStreamMute(AudioManager.STREAM_SYSTEM,
								// false);
								// amanager.setStreamMute(
								// AudioManager.STREAM_NOTIFICATION, false);

								// Intent intent = new
								// Intent(MainActivityTest.this,FinishActivity.class);
								// intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
								//
								// startActivity(intent);
							}
						}).show();

	}

	// private void startOptionsDialog() {
	// new AlertDialog.Builder(this)
	// .setTitle("")
	// .setMessage(
	// "Call filtering will start automatically when phone restarts.")
	// .setPositiveButton(R.string.str_ok,
	// new DialogInterface.OnClickListener() {
	// public void onClick(
	// DialogInterface dialoginterface, int i) {
	//
	// BootUpReceiver.newInstance();
	//
	// PackageManager pm = MainActivityTest.this
	// .getPackageManager();
	// ComponentName componentName = new ComponentName(
	// MainActivityTest.this,
	// BootUpReceiver.class);
	// pm.setComponentEnabledSetting(
	// componentName,
	// PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
	// PackageManager.DONT_KILL_APP);
	// Toast.makeText(getApplicationContext(),
	// "Activated", Toast.LENGTH_LONG).show();
	//
	// }
	// })
	// .setNegativeButton(R.string.str_no,
	// new DialogInterface.OnClickListener() {
	// public void onClick(
	// DialogInterface dialoginterface, int i) {
	//
	// PackageManager pm = MainActivityTest.this
	// .getPackageManager();
	// ComponentName componentName = new ComponentName(
	// MainActivityTest.this,
	// BootUpReceiver.class);
	// pm.setComponentEnabledSetting(
	// componentName,
	// PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
	// PackageManager.DONT_KILL_APP);
	// Log.e("componentName--disable", ""
	// + componentName);
	// Toast.makeText(getApplicationContext(),
	// "Deactivated", Toast.LENGTH_LONG)
	// .show();
	// }
	// }).show();
	//
	// }

	public void setModes() {

		modeList = db.fetchModeListByOrder();

		int modeNo = shPreferences.getInt("MODE_NO", 0);

		if (modeList != null) {
			for (int i = 0; i < modeList.size(); i++) {
				switch (modeList.get(i).getModeNumber()) {
				case 0:
					if (modeNo == 0) {
						setContactsByMode(modeList.get(i).getModeName());
						txtStartTitle.setVisibility(View.INVISIBLE);
						txtEndTitle.setVisibility(View.INVISIBLE);
						txtStart.setVisibility(View.INVISIBLE);
						txtEnd.setVisibility(View.INVISIBLE);
						relDays.setVisibility(View.INVISIBLE);

						frm1.setBackgroundResource(R.drawable.menu1);
						frm2.setBackgroundResource(R.drawable.menu2);
						frm3.setBackgroundResource(R.drawable.menu3);
						frm4.setBackgroundResource(R.drawable.menu4);

						// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
						// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
						// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
						// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

					}
					break;
				case 1:
					txtMode1.setText(modeList.get(i).getModeName());
					if (modeNo == 1) {

						txtStartTitle.setVisibility(View.VISIBLE);
						txtEndTitle.setVisibility(View.VISIBLE);
						txtStart.setVisibility(View.VISIBLE);
						txtEnd.setVisibility(View.VISIBLE);
						relDays.setVisibility(View.VISIBLE);

						txtStart.setText(modeList
								.get(i)
								.getStartTime()
								.substring(0,
										modeList.get(i).getStartTime().length())
								.replaceAll(" ", "").toLowerCase());
						txtEnd.setText(modeList
								.get(i)
								.getEndTime()
								.substring(0,
										modeList.get(i).getEndTime().length())
								.replaceAll(" ", "").toLowerCase());
						setDays(modeList.get(i).getDay());

						txtMode1.setText(modeList.get(i).getModeName());
						// txtExit1.setVisibility(View.VISIBLE);
						txtMode1.setTag("ON");
						txtHeader.setText(modeList.get(i).getModeName());
						setContactsByMode(modeList.get(i).getModeName());

						frm1.setBackgroundResource(R.drawable.slct_new);
						frm2.setBackgroundResource(R.drawable.menu2);
						frm3.setBackgroundResource(R.drawable.menu3);
						frm4.setBackgroundResource(R.drawable.menu4);

						// frm1.setBackground(getResources().getDrawable(R.drawable.slct_new));
						// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
						// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
						// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

					} else {
						txtMode1.setTag("OFF");
					}
					break;
				case 2:
					txtMode2.setText(modeList.get(i).getModeName());
					if (modeNo == 2) {

						txtStartTitle.setVisibility(View.VISIBLE);
						txtEndTitle.setVisibility(View.VISIBLE);
						txtStart.setVisibility(View.VISIBLE);
						txtEnd.setVisibility(View.VISIBLE);
						relDays.setVisibility(View.VISIBLE);

						txtStart.setText(modeList
								.get(i)
								.getStartTime()
								.substring(0,
										modeList.get(i).getStartTime().length())
								.replaceAll(" ", "").toLowerCase());
						txtEnd.setText(modeList
								.get(i)
								.getEndTime()
								.substring(0,
										modeList.get(i).getEndTime().length())
								.replaceAll(" ", "").toLowerCase());
						setDays(modeList.get(i).getDay());
						;

						txtMode2.setText(modeList.get(i).getModeName());
						// txtExit2.setVisibility(View.VISIBLE);
						txtMode2.setTag("ON");
						txtHeader.setText(modeList.get(i).getModeName());
						setContactsByMode(modeList.get(i).getModeName());

						frm1.setBackgroundResource(R.drawable.menu1);
						frm2.setBackgroundResource(R.drawable.slct_new2);
						frm3.setBackgroundResource(R.drawable.menu3);
						frm4.setBackgroundResource(R.drawable.menu4);

						// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
						// frm2.setBackground(getResources().getDrawable(R.drawable.slct_new2));
						// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
						// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

					} else {
						txtMode2.setTag("OFF");
					}
					break;
				case 3:
					txtMode3.setText(modeList.get(i).getModeName());
					if (modeNo == 3) {

						txtStartTitle.setVisibility(View.VISIBLE);
						txtEndTitle.setVisibility(View.VISIBLE);
						txtStart.setVisibility(View.VISIBLE);
						txtEnd.setVisibility(View.VISIBLE);
						relDays.setVisibility(View.VISIBLE);

						txtStart.setText(modeList
								.get(i)
								.getStartTime()
								.substring(0,
										modeList.get(i).getStartTime().length())
								.replaceAll(" ", "").toLowerCase());
						txtEnd.setText(modeList
								.get(i)
								.getEndTime()
								.substring(0,
										modeList.get(i).getEndTime().length())
								.replaceAll(" ", "").toLowerCase());
						setDays(modeList.get(i).getDay());

						txtMode3.setText(modeList.get(i).getModeName());
						// txtExit3.setVisibility(View.VISIBLE);
						txtMode3.setTag("ON");
						txtHeader.setText(modeList.get(i).getModeName());
						setContactsByMode(modeList.get(i).getModeName());

						frm1.setBackgroundResource(R.drawable.menu1);
						frm2.setBackgroundResource(R.drawable.menu2);
						frm3.setBackgroundResource(R.drawable.slct_new3);
						frm4.setBackgroundResource(R.drawable.menu4);

						// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
						// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
						// frm3.setBackground(getResources().getDrawable(R.drawable.slct_new3));
						// frm4.setBackground(getResources().getDrawable(R.drawable.menu4));

					} else {
						txtMode3.setTag("OFF");
					}
					break;
				case 4:
					txtMode4.setText(modeList.get(i).getModeName());
					if (modeNo == 4) {

						txtStartTitle.setVisibility(View.VISIBLE);
						txtEndTitle.setVisibility(View.VISIBLE);
						txtStart.setVisibility(View.VISIBLE);
						txtEnd.setVisibility(View.VISIBLE);
						relDays.setVisibility(View.VISIBLE);

						txtStart.setText(modeList
								.get(i)
								.getStartTime()
								.substring(0,
										modeList.get(i).getStartTime().length())
								.replaceAll(" ", "").toLowerCase());
						txtEnd.setText(modeList
								.get(i)
								.getEndTime()
								.substring(0,
										modeList.get(i).getEndTime().length())
								.replaceAll(" ", "").toLowerCase());
						setDays(modeList.get(i).getDay());
						;

						txtMode4.setText(modeList.get(i).getModeName());
						// txtExit4.setVisibility(View.VISIBLE);
						txtMode4.setTag("ON");
						txtHeader.setText(modeList.get(i).getModeName());
						setContactsByMode(modeList.get(i).getModeName());

						frm1.setBackgroundResource(R.drawable.menu1);
						frm2.setBackgroundResource(R.drawable.menu2);
						frm3.setBackgroundResource(R.drawable.menu3);
						frm4.setBackgroundResource(R.drawable.slct_new4);

						// frm1.setBackground(getResources().getDrawable(R.drawable.menu1));
						// frm2.setBackground(getResources().getDrawable(R.drawable.menu2));
						// frm3.setBackground(getResources().getDrawable(R.drawable.menu3));
						// frm4.setBackground(getResources().getDrawable(R.drawable.slct_new4));
						// frm4.setBackground(getResources().getDrawable(
						// R.drawable.slct4));

					} else {
						txtMode4.setTag("OFF");
					}
					break;
				}
			}
		} else {
			b1.setEnabled(false);
			Log.i("Executed", "Success");
		}

	}

	private void setContacts() {

		contactList = db.fetchContactListByMode("General");

		ArrayList<MyContact> existingContacts = new ArrayList<MyContact>();

		if (contactList == null) {
			contactList = new ArrayList<MyContact>();

			Log.e("", "" + contactList);
		}

		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
		strTime = sdf.format(c.getTime());

		String dotSplitStart[] = new String[2];

		dotSplitStart = strTime.split(":");

		String apSplitStart[] = new String[2];

		apSplitStart = dotSplitStart[1].split(" ");

		int hoursStart = 0;
		int minStart = Integer.parseInt(apSplitStart[0]);

		if (apSplitStart[1].equals("AM")) {
			hoursStart = Integer.parseInt(dotSplitStart[0]);
		} else {
			hoursStart = Integer.parseInt(dotSplitStart[0]) + 12;
		}

		int totalStartTime = (hoursStart * 60) + minStart;

		for (int i = 0; i < contactList.size(); i++) {
			stringend = contactList.get(i).getEndTime();
			stringstart = contactList.get(i).getStartTime();
			// poilsi = position;
			// compare = stringend;
			// Log.e("stringend", "" + stringend);
			// Log.e("strTime", "" + strTime);

			String dotSplitEnd[] = new String[2];

			dotSplitEnd = stringend.split(":");

			String apSplitEnd[] = new String[2];

			apSplitEnd = dotSplitEnd[1].split(" ");

			int hoursEnd = 0;
			int minEnd = Integer.parseInt(apSplitEnd[0]);

			if (apSplitEnd[1].equals("AM")) {

				if ((apSplitEnd[1].equals("AM") && stringstart.contains("PM") && hoursEnd <= 10)
						|| (apSplitEnd[1].equals("AM")
								&& stringstart.contains("AM") && hoursEnd <= 10)) {
					hoursEnd = Integer.parseInt(dotSplitEnd[0]) + 22;
					Log.e("hereeeee000", "" + hoursEnd);

				} else {
					hoursEnd = Integer.parseInt(dotSplitEnd[0]);
				}
			} else {
				hoursEnd = Integer.parseInt(dotSplitEnd[0]) + 12;
			}

			int totalEndTime = (hoursEnd * 60) + minEnd;

			// Log.e("start_end", "" + totalEndTime + "  " + totalStartTime);

			if (txtHeader.getText().toString().equalsIgnoreCase("General")
					&& totalEndTime <= totalStartTime) {

				MainActivityTest.db.deleteContact(contactList.get(i).getId());

				contactList.clear();
				// myListAdapter.notifyDataSetChanged();

				Log.e("delete", "deleted");

			} else {

				existingContacts.add(contactList.get(i));

				Log.e("here else", "notdeleted");
			}
		}
		Log.e("1 else", "1111");
		// myListAdapter = new MyListAdapter(existingContacts);

		myListAdapter = new MyListAdapter(contactList);
		listView.setAdapter(myListAdapter);
		myListAdapter.notifyDataSetChanged();

	}

	private void setContactsByMode(String modeName) {

		// Log.e("2 else", "2222");

		contactList = db.fetchContactListByMode(modeName);

		if (contactList != null) {
			myListAdapter = new MyListAdapter(contactList);
			listView.setAdapter(myListAdapter);
			myListAdapter.notifyDataSetChanged();

			// }
			//
			// else {
			// contactList = new ArrayList<MyContact>();
			// myListAdapter = new MyListAdapter(contactList);
			// listView.setAdapter(myListAdapter);
			// myListAdapter.notifyDataSetChanged();
			// }
			// }

			ArrayList<MyContact> existingContacts = new ArrayList<MyContact>();

			Calendar c = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
			strTime = sdf.format(c.getTime());

			String dotSplitStart[] = new String[2];

			dotSplitStart = strTime.split(":");

			String apSplitStart[] = new String[2];

			apSplitStart = dotSplitStart[1].split(" ");

			int hoursStart = 0;
			int minStart = Integer.parseInt(apSplitStart[0]);

			if (apSplitStart[1].equals("AM")) {
				hoursStart = Integer.parseInt(dotSplitStart[0]);
			} else {
				hoursStart = Integer.parseInt(dotSplitStart[0]) + 12;
			}

			// Log.e("hourstart", "" + hoursStart);
			// Log.e("minstart", "" + minStart);
			//
			int totalStartTime = (hoursStart * 60) + minStart;

			for (int i = 0; i < contactList.size(); i++) {
				stringend = contactList.get(i).getEndTime();
				stringstart = contactList.get(i).getStartTime();

				// poilsi = position;
				// compare = stringend;

				// Log.e("systemTime", "" + strTime);
				// Log.e("stringend", "" + stringend);

				String dotSplitEnd[] = new String[2];

				dotSplitEnd = stringend.split(":");

				String apSplitEnd[] = new String[2];

				apSplitEnd = dotSplitEnd[1].split(" ");

				int hoursEnd = 0;
				int minEnd = Integer.parseInt(apSplitEnd[0]);
				// Log.e("minEnd", "" + apSplitEnd[0]+","+apSplitEnd[1]);
				if (apSplitEnd[1].equals("AM")) {

					if ((apSplitEnd[1].equals("AM")
							&& stringstart.contains("PM") && hoursEnd <= 10)
							|| (apSplitEnd[1].equals("AM")
									&& stringstart.contains("AM") && hoursEnd <= 10)) {
						hoursEnd = Integer.parseInt(dotSplitEnd[0]) + 22;
						Log.e("hereeeee000", "" + hoursEnd);

					} else {
						hoursEnd = Integer.parseInt(dotSplitEnd[0]);
					}
				}

				else {

					hoursEnd = Integer.parseInt(dotSplitEnd[0]) + 12;
				}

				int totalEndTime = (hoursEnd * 60) + minEnd;

				Log.e("hereeeee1111", "" + hoursEnd);
				Log.e("start_enddddddddddddddddd", "" + totalEndTime + "  "
						+ totalStartTime);

				try {
					if (txtHeader.getText().toString()
							.equalsIgnoreCase("General")
							&& totalEndTime <= totalStartTime) {

						Log.e("modetodelete", ""
								+ txtHeader.getText().toString());

						MainActivityTest.db.deleteContact(contactList.get(i)
								.getId());

						contactList.clear();
						// myListAdapter.notifyDataSetChanged();

						Log.e("delete", "deleted");

					} else {

						existingContacts.add(contactList.get(i));

						// Log.e("here else", "notdeleted");
					}
				} catch (Exception e) {

				}

			}

		}

		else {
			contactList = new ArrayList<MyContact>();
			myListAdapter = new MyListAdapter(contactList);
			listView.setAdapter(myListAdapter);
			myListAdapter.notifyDataSetChanged();

		}

		if (txtHeader.getText().toString().equalsIgnoreCase("General")
				&& contactList.isEmpty()) {
			// Log.e("isempty", "isempty");
			layoutlist.setBackgroundResource(R.drawable.general_backgrd_image);
		} else {
			// Log.e("isNottttttempty", "isnottttttttttttempty");
			layoutlist.setBackgroundResource(R.drawable.background);
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// showNotification();
	}

	@Override
	protected void onDestroy() {
		db.close();

		super.onDestroy();

	}

	class MyListAdapter extends BaseAdapter {

		private List<MyContact> datarow = null;
		private TextView txtContactName;
		private TextView txtStartTime;
		private TextView txtEndTime;
		private TextView txtDuration;
		private TextView txtSleep;
		private TextView txtMode;

		public MyListAdapter(List<MyContact> datarow) {

			this.datarow = datarow;

		}

		public int getCount() {
			// TODO Auto-generated method stub
			if (datarow == null) {
				return 0;
			} else {
				return datarow.size();
			}

		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return datarow.get(position);
		}

		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.contact_row, null);
			}

			String sessionTimeStart = datarow.get(position).getStartTime();
			String sessionTimerEndOrCurrent = datarow.get(position)
					.getEndTime();

			SimpleDateFormat format = new SimpleDateFormat("hh:mm a");

			Date d1 = null;
			Date d0 = null;
			try {
				d1 = format.parse(sessionTimeStart);
				d0 = format.parse(sessionTimerEndOrCurrent);

				String strt = datarow.get(position).getStartTime();
				String end = datarow.get(position).getEndTime();

				int startHours = 0, startMin = 0, endHours = 0, endMin = 0;

				if (strt.contains("AM")) {
					startHours = Integer
							.parseInt(strt.split(" ")[0].split(":")[0]);
					startMin = Integer
							.parseInt(strt.split(" ")[0].split(":")[1]);
					if (startHours == 12) {
						startHours = 0;
					}
				}

				if (strt.contains("PM")) {
					startHours = Integer
							.parseInt(strt.split(" ")[0].split(":")[0]);
					startMin = Integer
							.parseInt(strt.split(" ")[0].split(":")[1]);
					if (startHours != 12) {
						startHours += 12;
					}
				}

				if (end.contains("AM")) {
					endHours = Integer
							.parseInt(end.split(" ")[0].split(":")[0]);
					endMin = Integer.parseInt(end.split(" ")[0].split(":")[1]);
					if (endHours == 12) {
						endHours = 0;
					}
				}

				if (end.contains("PM")) {
					endHours = Integer
							.parseInt(end.split(" ")[0].split(":")[0]);
					endMin = Integer.parseInt(end.split(" ")[0].split(":")[1]);
					if (endHours != 12) {
						endHours += 12;
					}
				}

				int startTotalMinutes = (startHours * 60) + startMin;
				int endTotalMinutes = (endHours * 60) + endMin;

				int totalDayMinutes = 24 * 60;

				int totalMinDIff = 0;

				if (endTotalMinutes <= startTotalMinutes) {
					totalMinDIff = totalDayMinutes
							- (startTotalMinutes - endTotalMinutes);
				} else {
					totalMinDIff = endTotalMinutes - startTotalMinutes;
				}

				hourDiff = (int) (totalMinDIff / 60);

				minDiff = (int) (totalMinDIff % 60);

				Log.e("startHours", "" + startHours);
				Log.e("endHours", "" + endHours);
				Log.e("startTotalMinutes", "" + startTotalMinutes);
				Log.e("endTotalMinutes", "" + endTotalMinutes);

				// if (strt.contains("PM") && end.contains("AM")) {
				// // pm to am ,am to am
				//
				// Calendar calendar1 = Calendar.getInstance();
				// calendar1.setTime(d1);
				//
				// Calendar calendar2 = Calendar.getInstance();
				// calendar2.setTime(d0);
				// calendar2.add(Calendar.DATE, 1);
				//
				// Date x = calendar1.getTime();
				// Date xy = calendar2.getTime();
				//
				// long diff = xy.getTime() - x.getTime();
				// long mindifference1 = diff / (60 * 1000);
				// float hoursDifference = mindifference1 / 60;
				//
				// int hours_Difference = (int) Math.abs(hoursDifference);
				// difference = Integer.toString(hours_Difference);
				//
				// int diffMinutes = (int) (diff) / (60 * 1000) % 60;
				// int diff_Minutes = Math.abs(diffMinutes);
				//
				// mindifference = Integer.toString(diff_Minutes);
				//
				// Log.e("diffHours", "" + difference);
				// Log.e("mindifference", "" + mindifference);
				//
				// } else {

				/*
				 * int hoursDifference = (int) ((d0.getTime() - d1.getTime()) /
				 * (60 * 60 * 1000));
				 * 
				 * int diffMinutes = (int) ((d0.getTime() - d1.getTime())/ (60 *
				 * 1000) % 60) ;
				 * 
				 * Log.e("hoursDifferenceelseeee", "" +
				 * hoursDifference+","+diffMinutes);
				 * 
				 * if ( hoursDifference < 0 ) {
				 * 
				 * difference = Integer.toString(24 + hoursDifference);
				 * 
				 * mindifference = Integer.toString(diffMinutes);
				 * Log.e("difference", "" + difference); } else{ // int
				 * hours_Difference = Math.abs(hoursDifference); difference =
				 * Integer.toString(hoursDifference); mindifference =
				 * Integer.toString(diffMinutes); }
				 */

				// int diff_Minutes = Math.abs(diffMinutes);

				// Log.e("difference", "" + difference);
				//
				// Log.e("min", ""+mindifference );
				// }

				//
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				Log.e("ParseException", "ParseException");

			}

			txtContactName = (TextView) convertView
					.findViewById(R.id.title_name);
			txtStartTime = (TextView) convertView.findViewById(R.id.start);
			txtEndTime = (TextView) convertView.findViewById(R.id.end);
			txtDuration = (TextView) convertView.findViewById(R.id.duration);
			txtSleep = (TextView) convertView.findViewById(R.id.silent);
			txtMode = (TextView) convertView.findViewById(R.id.mode);

			txtContactName.setText(datarow.get(position).getContactName());

			txtStartTime.setText(datarow.get(position).getStartTime());
			txtEndTime.setText(datarow.get(position).getEndTime());

			// txtDuration.setText(difference + " hr " + mindifference + "m");

			txtDuration.setText(hourDiff + " hr " + minDiff + "m");

			txtSleep.setText("  "
					+ datarow.get(position).getRingerOption().charAt(0));
			txtMode.setText(datarow.get(position).getModeName());
			// Log.e("gid", datarow.get(position).getGroupId());
			convertView.setBackgroundColor(Color.TRANSPARENT);

			return convertView;
		}
	}

	@SuppressWarnings("deprecation")
	private void showNotification() {

		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);

		int icon = R.drawable.notification_icon;
		CharSequence tickerText = "Priority Call Filter is On";
		long when = System.currentTimeMillis();

		Notification notification = new Notification(icon, tickerText, when);

		Context context = getApplicationContext();
		CharSequence contentTitle = "Priority Call Filter";
		CharSequence contentText = "Priority Call Filter is On";
		Intent notificationIntent = new Intent(this, MainActivityTest.class);
		// notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, contentTitle, contentText,
				contentIntent);
		notification.flags = Notification.FLAG_NO_CLEAR;
		mNotificationManager.notify(1, notification);
	}

	private void cancelNotification() {

		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
		mNotificationManager.cancel(1);
	}

	private void setDays(String daysString) {
		int[] dayValues = IOUtils.getIntArrayFromDayString(daysString);
		for (int i = 0; i < dayValues.length; i++) {
			switch (i) {
			case 0:
				if (dayValues[i] == 1)
					txtDays[i].setText("Su");
				else
					txtDays[i].setText("");

				break;
			case 1:
				if (dayValues[i] == 1)
					txtDays[i].setText("M");
				else
					txtDays[i].setText("");
				break;
			case 2:
				if (dayValues[i] == 1)
					txtDays[i].setText("Tu");
				else
					txtDays[i].setText("");
				break;
			case 3:
				if (dayValues[i] == 1)
					txtDays[i].setText("W");
				else
					txtDays[i].setText("");
				break;
			case 4:
				if (dayValues[i] == 1)
					txtDays[i].setText("Th");
				else
					txtDays[i].setText("");
				break;
			case 5:
				if (dayValues[i] == 1)
					txtDays[i].setText("F");
				else
					txtDays[i].setText("");
				break;
			case 6:
				if (dayValues[i] == 1)
					txtDays[i].setText("Sa");
				else
					txtDays[i].setText("");
				break;
			default:
				break;
			}
		}
	}

	// private void startNotification() {
	//
	// nManager = (NotificationManager)
	// getSystemService(Context.NOTIFICATION_SERVICE);
	//
	// notification = new Notification();
	//
	// Intent notificationIntent = new Intent();
	// // Use the UPDATE_CURRENT FLAG for this notification since, we would be
	// // changing the iconLevel of
	// PendingIntent contentIntent = PendingIntent.getActivity(this,
	// (int) System.currentTimeMillis(), notificationIntent,
	// PendingIntent.FLAG_UPDATE_CURRENT);
	//
	// RemoteViews contentView = new RemoteViews(getPackageName(),
	// R.layout.remote);
	//
	// notification.contentView = contentView;
	// notification.contentIntent = contentIntent;
	//
	// notification.flags = Notification.FLAG_ONGOING_EVENT;
	// notification.icon = R.drawable.level_list;
	//
	// notification.iconLevel = 0;
	//
	// nManager.notify(R.string.app_name, notification);
	//
	// Thread thread = new Thread(this);
	// thread.start();
	// }
	//
	// private void stopNotification() {
	// nManager.cancelAll();
	// }

	@Override
	public void run() {
		while (!isStopped) {
			try {
				Thread.sleep(100);
				handler.sendEmptyMessage(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			// myListAdapter = new MyListAdapter(contactList);
			// listView.setAdapter(myListAdapter);
			// if (strTime.equals(compare)) {
			//
			// Log.e("here if", "fsgv");
			// MainActivityTest.db.deleteContact(poilsi);
			// contactList.remove(poilsi);
			// myListAdapter.notifyDataSetChanged();
			// }

			if (notification.iconLevel < 3) {
				notification.iconLevel++;
			} else {
				notification.iconLevel = 0;
			}
			if (!isStopped) {
				nManager.notify(R.string.app_name, notification);
			} else {
				nManager.cancel(R.string.app_name);
			}
			if (!isStopped) {
				nManager.notify(R.string.app_name, notification);
			} else {
				nManager.cancel(R.string.app_name);
			}
			super.handleMessage(msg);
		}

	};

	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	public void openMenu() {
		openOptionsMenu();
	}
	// protected void setMenuBackground(){
	// // Log.d(TAG, "Enterting setMenuBackGround");
	// getLayoutInflater().setFactory( new Factory() {
	// public View onCreateView(String name, Context context, AttributeSet
	// attrs) {
	// if ( name.equalsIgnoreCase(
	// "com.android.internal.view.menu.IconMenuItemView" ) ) {
	// try { // Ask our inflater to create the view
	// LayoutInflater f = getLayoutInflater();
	// final View view = f.createView( name, null, attrs );
	// /* The background gets refreshed each time a new item is added the
	// options menu.
	// * So each time Android applies the default background we need to set our
	// own
	// * background. This is done using a thread giving the background change as
	// runnable
	// * object */
	// new Handler().post( new Runnable() {
	// public void run () {
	// // sets the background color
	// view.setBackgroundResource( R.color.textcolor);
	// // sets the text color
	// ((TextView) view).setTextColor(Color.GREEN);
	// // sets the text size
	// ((TextView) view).setTextSize(18);
	// }
	// } );
	// return view;
	// }
	// catch ( InflateException e ) {}
	// catch ( ClassNotFoundException e ) {}
	// }
	// return null;
	// }});
	// }

}