package pro.com.data;

import android.content.Context;

public class ConstantData {

	public static MyContact myContact = null;
	public static Mode mode = null;
	public static Context context;
	public static boolean isFilteringOn = true;
	public static int ringerModeFilteringOff = 2;
	public static int ringerModeFilteringOn = 2;
}
