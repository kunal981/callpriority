package pro.com;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import pro.com.data.ConstantData;
import pro.com.data.IOUtils;
import pro.com.data.Mode;
import pro.com.data.MyContact;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

public class ContactSetting extends Activity implements OnCheckedChangeListener {
	Spinner sp;
	String id;
	Long mRowId;
	Button submit;
	Spinner spmode, spinnerMode;
	String[] name;
	EditText name1, start_time, end_time, edtHours, edtMins;
	private String start, end;
	private boolean isDuration = false;
	private int mHour;
	private int mMinute;
	private int mHourtwo;
	private int mMinutetwo;
	private int mHourDuration;
	private int mMinuteDuration;
	String phonenumber;
	private int mMinutethree;
	private int mHourthree;
	CheckBox chkDuration;
	CheckBox chkTime;
	int starttimeedit = 0;
	int endtimeedit = 0;
	int[] days = new int[10];

	String act2MyValue;
	String groupname;
	String durationString = null;
	static final int TIME_DIALOG_ID_ONE = 0;
	static final int TIME_DIALOG_ID_TWO = 1;
	static final int TIME_DIALOG_ID_THREE = 2;
	static final int TIME_DIALOG_ID_DURATION = 5;
	static final int DIALOG_ID_ERROR = 3;
	static final int DIALOG_ID_TIME = 4;
	int i = 0;
	EditText editContact;
	EditText mode;
	String startTime, endTime;
	EditText vibratemode;
	private String[] array_spinner;
	private String[] array_spinner_ring = { "Vibrate", "Ring" };
	ArrayAdapter adapter2;
	private MyContact myContact;
	private ArrayList<Mode> modeList;
	private Button btnDelete;
	private String currentMode;
	private int currentModePosition = 0, currentRingerPosition = 0;
	private String phoneNumber;
	private String startTimeExtra;
	private String endTimeExtra, ringtypeExtra;
	private boolean isContactInseretd = false;

	ArrayList<String> arrayListName;

	@SuppressWarnings("unchecked")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		currentMode = getIntent().getExtras().getString("currentMode");

		if (ConstantData.myContact != null) {
			myContact = ConstantData.myContact;
			ConstantData.myContact = null;
		}

		if (myContact == null) {
			setContentView(R.layout.contactsettings03);
		} else {
			setContentView(R.layout.contactsettings02);
			btnDelete = (Button) findViewById(R.id.Button02);
			btnDelete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					btnDelete.setBackground(getResources().getDrawable(
							R.drawable.btn_slct));
					deleteContactDialog();
				}
			});
		}

		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		Bundle bundle = getIntent().getExtras();
		if (myContact == null) {
			act2MyValue = bundle.getString("str1");

			// groupname=bundle.getString("strgroup");
			phoneNumber = bundle.getString("phoneNumber");
			startTimeExtra = bundle.getString("start_time");
			endTimeExtra = bundle.getString("end_time");
			ringtypeExtra = bundle.getString("ring_type");
		} else {
			phoneNumber = myContact.getPhoneNumber();
			// Log.e("phoneNumber:", "" + phoneNumber);
		}

		editContact = (EditText) findViewById(R.id.et1);
		start_time = (EditText) findViewById(R.id.start_time);
		chkDuration = (CheckBox) findViewById(R.id.cb);
		chkTime = (CheckBox) findViewById(R.id.c2);
		chkDuration.setOnCheckedChangeListener(this);
		chkTime.setOnCheckedChangeListener(this);
		end_time = (EditText) findViewById(R.id.end_time);
		edtHours = (EditText) findViewById(R.id.edtHours);
		edtMins = (EditText) findViewById(R.id.edtMins);
		editContact.setFocusable(false);

		spmode = (Spinner) findViewById(R.id.viratering);

		arrayListName = new ArrayList<String>();
		arrayListName.add("Vibrate");
		arrayListName.add("Ring");

		// SpinnerAdapter_ sadapter = new
		// SpinnerAdapter_(getApplicationContext(),
		// R.layout.spinner_layout, arrayListName);
		// // Log.e("arraylist", ""+arrayListName);
		// spmode.setAdapter(sadapter);
		//

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.items2, android.R.layout.simple_spinner_item);

		// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spmode = (Spinner) findViewById(R.id.viratering);

		// adapter = ArrayAdapter.createFromResource(this, R.array.items1,
		// android.R.layout.simple_spinner_item);

		adapter = ArrayAdapter.createFromResource(this, R.array.items1,
				R.layout.spinner_mode);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spmode.setAdapter(adapter);

		spinnerMode = (Spinner) findViewById(R.id.Spinner01);

		modeList = MainActivityTest.db.fetchModeListByOrder();

		array_spinner = new String[modeList.size()];
		for (int i = 0; i < modeList.size(); i++) {
			array_spinner[i] = modeList.get(i).getModeName();
			if (array_spinner[i].equalsIgnoreCase(currentMode)) {
				currentModePosition = i;
			}
		}

		// SpinnerModeAdapter sadapter2 = new
		// SpinnerModeAdapter(getApplicationContext(),
		// R.layout.spinner_layout, array_spinner);
		// Log.e("array_spinner", ""+array_spinner.length);
		//
		// spinnerMode.setAdapter(sadapter2);
		//

		adapter2 = new ArrayAdapter(this, R.layout.spinner_mode, array_spinner);

		// {
		// @Override
		// public View getDropDownView(int position, View convertView,
		// ViewGroup parent) {
		// // TODO Auto-generated method stub
		// return super.getDropDownView(position, convertView, parent);
		// }
		// };
		// adapter2.setDropDownViewResource(R.layout.spinner_mode_dropdown);

		// adapter2 = new ArrayAdapter(this,
		// android.R.layout.simple_spinner_item,
		// array_spinner);

		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerMode.setAdapter(adapter2);

		spinnerMode.setSelection(currentModePosition);

		if (myContact == null) {

			editContact.setText("" + act2MyValue);

			// editContact.setText(" "+groupname);

			// Log.e("groupname", ""+groupname);

			start_time.setText(startTimeExtra);
			end_time.setText(endTimeExtra);

			for (int i = 0; i < 2; i++) {
				if (array_spinner_ring[i].equalsIgnoreCase(ringtypeExtra)) {
					currentRingerPosition = i;
				}
			}
			spmode.setSelection(currentRingerPosition);
		}

		editContact.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent it = new Intent(ContactSetting.this, Contactlist1.class);

				it.putExtra("currentMode", currentMode);
				it.putExtra("start_time", startTimeExtra);
				it.putExtra("end_time", endTimeExtra);
				it.putExtra("ring_type", ringtypeExtra);
				finish();
				startActivity(it);

			}
		});

		final Calendar c = Calendar.getInstance();
		mHour = c.get(Calendar.HOUR_OF_DAY);
		mMinute = c.get(Calendar.MINUTE);
		mHourtwo = c.get(Calendar.HOUR_OF_DAY);
		mMinutetwo = c.get(Calendar.MINUTE);
		mHourthree = c.get(Calendar.HOUR_OF_DAY);
		mMinutethree = c.get(Calendar.MINUTE);

		start_time.setFocusable(false);

		start_time.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// showDialog(TIME_DIALOG_ID_ONE);
				Intent intent = new Intent(ContactSetting.this,
						TimePickerActivity.class);

				intent.putExtra("time", "none");

				startActivityForResult(intent, 11);
			}
		});

		end_time.setFocusable(false);
		start_time.setFocusable(false);
		submit = (Button) findViewById(R.id.Button01);

		submit.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {

				submit.setBackground(getResources().getDrawable(
						R.drawable.btn_slct));

				// String generalMode=spinnerMode.getSelectedItem().toString();
				// String timeStart=start_time.getText().toString();
				// String timeEnd=end_time.getText().toString();
				//
				// if(generalMode.equalsIgnoreCase("General") &&
				// timeStart.contains("PM")&& timeEnd.contains("AM")){
				//
				// selectNextDayDialog();
				// Log.e("yessssssssssssss", "yessssssssssssssss");
				// }else{

				if (edtHours.getText().toString().equalsIgnoreCase("")
						|| edtMins.getText().toString().equalsIgnoreCase("")) {
					if (start_time.getText().toString().equalsIgnoreCase("")
							|| end_time.getText().toString()
									.equalsIgnoreCase("")) {
						fieldBlankDialog();
					} else {

						insertContact(false);
					}
				} else {

					insertContact(true);
				}

				// }

			}

		});

		end_time.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// showDialog(TIME_DIALOG_ID_TWO);
				Intent intent = new Intent(ContactSetting.this,
						TimePickerActivity.class);

				intent.putExtra("time", "none");
				startActivityForResult(intent, 12);
			}
		});

		edtHours.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// showDialog(TIME_DIALOG_ID_DURATION);
				Intent intent = new Intent(ContactSetting.this,
						HourPickerActivity.class);
				startActivityForResult(intent, 13);
			}
		});

		edtMins.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// showDialog(TIME_DIALOG_ID_DURATION);
				Intent intent = new Intent(ContactSetting.this,
						MinutesPickerActivity.class);
				startActivityForResult(intent, 14);
			}
		});

		chkTime.setChecked(true);
		edtHours.setText("");
		edtMins.setText("");
		edtHours.setEnabled(false);
		edtMins.setEnabled(false);
		edtHours.setClickable(false);
		edtMins.setClickable(false);
		edtHours.setFocusable(false);
		edtMins.setFocusable(false);
		start_time.setText("");
		start_time.setEnabled(true);
		end_time.setText("");
		end_time.setEnabled(true);

		if (myContact != null) {
			setContactInfo();
		} else {
			setContactInfoFromMode();
		}

	}

	private void setContactInfo() {
		editContact.setText(myContact.getContactName());
		start_time.setText(myContact.getStartTime());
		end_time.setText(myContact.getEndTime());
		if (myContact.getRingerOption().equalsIgnoreCase("Vibrate")) {
			spmode.setSelection(0);
		} else {
			spmode.setSelection(1);
		}
		for (int i = 0; i < array_spinner.length; i++) {
			if (myContact.getModeName().equalsIgnoreCase(array_spinner[i])) {
				spinnerMode.setSelection(i);
			}
		}
	}

	private void setContactInfoFromMode() {

		Mode mode = MainActivityTest.db.fetchModeByName(currentMode);

		Log.e("modesetcontact", "" + mode);

		if (!mode.getModeName().equalsIgnoreCase("General")) {
			start_time.setText(mode.getStartTime());
			end_time.setText(mode.getEndTime());
		} else {
			start_time.setText("");
			end_time.setText("");
		}

		for (int i = 0; i < array_spinner.length; i++) {
			if (mode.getModeName().equalsIgnoreCase(array_spinner[i])) {
				spinnerMode.setSelection(i);
			}
		}
		for (int i = 0; i < array_spinner_ring.length; i++) {
			if (mode.getModeName().equalsIgnoreCase(array_spinner_ring[i])) {
				spmode.setSelection(i);
			}
		}

	}

	private boolean insertContact(boolean isDuration) {
		this.isDuration = isDuration;
		boolean isOverlapping = false;
		ArrayList<Date> dateList = null;
		MyContact tempContact = new MyContact();

		try {
			tempContact.setContactName(editContact.getText().toString());

			start = start_time.getText().toString();
			end = end_time.getText().toString();

			if (isDuration) {

				dateList = getStart_EndTimeFromDuration(edtHours.getText()
						.toString().substring(0, 2).trim()
						+ ":"
						+ edtMins.getText().toString().substring(0, 2).trim());

				// start time
				int mMeasureHourTwo = dateList.get(0).getHours();
				int mMeasureMinuteTwo = dateList.get(0).getMinutes();
				if (mMeasureHourTwo > 12) {
					mMeasureHourTwo = mMeasureHourTwo - 12;
					start = IOUtils.getTimeString(mMeasureHourTwo) + ":"
							+ IOUtils.getTimeString(mMeasureMinuteTwo) + " PM";
				} else if (mMeasureHourTwo == 12) {
					start = IOUtils.getTimeString(mMeasureHourTwo) + ":"
							+ IOUtils.getTimeString(mMeasureMinuteTwo) + " PM";
				} else if (mMeasureHourTwo == 0) {
					mMeasureHourTwo = mMeasureHourTwo + 12;
					start = IOUtils.getTimeString(mMeasureHourTwo) + ":"
							+ IOUtils.getTimeString(mMeasureMinuteTwo) + " AM";
				} else {
					start = IOUtils.getTimeString(mMeasureHourTwo) + ":"
							+ IOUtils.getTimeString(mMeasureMinuteTwo) + " AM";
				}

				// end time
				int mMeasureHour = dateList.get(1).getHours();
				int mMeasureMinute = dateList.get(1).getMinutes();
				if (mMeasureHour > 12) {
					mMeasureHour = mMeasureHour - 12;
					end = IOUtils.getTimeString(mMeasureHour) + ":"
							+ IOUtils.getTimeString(mMeasureMinute) + " PM";
				} else if (mMeasureHour == 12) {
					end = IOUtils.getTimeString(mMeasureHour) + ":"
							+ IOUtils.getTimeString(mMeasureMinute) + " PM";
				} else if (mMeasureHour == 0) {
					mMeasureHour = mMeasureHour + 12;
					end = IOUtils.getTimeString(mMeasureHour) + ":"
							+ IOUtils.getTimeString(mMeasureMinute) + " AM";
				} else {
					end = IOUtils.getTimeString(mMeasureHour) + ":"
							+ IOUtils.getTimeString(mMeasureMinute) + " AM";
				}

				tempContact.setStartTime(start);
				tempContact.setEndTime(end);
			}
			tempContact.setStartTime(start);
			tempContact.setEndTime(end);
			tempContact.setRingerOption(spmode.getSelectedItem().toString());
			tempContact.setModeName(spinnerMode.getSelectedItem().toString());
			tempContact.setPhoneNumber(phoneNumber);
			if (myContact != null) {
				tempContact.setId(myContact.getId());
			} else {
				tempContact.setId(-1);
			}

			ArrayList<MyContact> compareContactList = MainActivityTest.db
					.fetchContactList();
			if (compareContactList != null) {
				for (int i = 0; i < compareContactList.size(); i++) {
					MyContact tempContact2 = compareContactList.get(i);
					if (tempContact.getId() == tempContact2.getId()) {
						isOverlapping = false;
					} else if (tempContact.getPhoneNumber().equalsIgnoreCase(
							tempContact2.getPhoneNumber())) {
						isOverlapping = isOverlapping(tempContact, tempContact2);
					}
					if (isOverlapping) {
						break;
					}
				}
			}

			isContactInseretd = !isOverlapping;

			if (isContactoutOfMode(tempContact,
					MainActivityTest.db.fetchModeByName(currentMode))) {
				contactoutOfModeDialog(isOverlapping);
			} else {
				goAhead(isOverlapping);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (!isOverlapping);
	}

	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// Toast.makeText(ExampleApp.this, "Time is="+hourOfDay+":"+minute,
			// Toast.LENGTH_SHORT).show();
			mHour = hourOfDay;
			mMinute = minute;
			mHourtwo = hourOfDay;
			mMinutetwo = minute;
			Log.i("",
					"************** VLAUE OF Measure Hour two ***************"
							+ mHourtwo);
			updateDisplay();
		}
	};

	private TimePickerDialog.OnTimeSetListener mTimeSetListenertwo = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

			mHourtwo = hourOfDay;
			mMinutetwo = minute;
			Log.i("",
					"************** VLAUE OF Measure Hour two ***************"
							+ mHourtwo);
			updateDisplaytwo();
		}
	};

	// private TimePickerDialog.OnTimeSetListener mTimeSetListenerDuration = new
	// TimePickerDialog.OnTimeSetListener() {
	// public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	//
	// mHourDuration = hourOfDay;
	// mMinuteDuration = minute;
	// Log.i("",
	// "************** VLAUE OF Measure Hour two ***************"
	// + mHourtwo);
	// updateDisplayDuration();
	// }
	// };

	// private TimePickerDialog.OnTimeSetListener mTimeSetListenerthree = new
	// TimePickerDialog.OnTimeSetListener() {
	// public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	//
	// mHourthree = hourOfDay;
	// mMinutethree = minute;
	// Log.i("",
	// "************** VLAUE OF Measure Hour two ***************"
	// + mHourtwo);
	//
	// updateDisplaythree();
	// }
	// };

	// protected Dialog onCreateDialog(int id) {
	// switch (id) {
	// case TIME_DIALOG_ID_ONE:
	// return new TimePickerDialog(this, mTimeSetListener, mHour, mMinute,
	// false);
	// case TIME_DIALOG_ID_TWO:
	// return new TimePickerDialog(this, mTimeSetListenertwo, mHourtwo,
	// mMinutetwo, false);
	// case TIME_DIALOG_ID_THREE:
	// return new TimePickerDialog(this, mTimeSetListenerthree,
	// mHourthree, mMinutethree, false);
	// case TIME_DIALOG_ID_DURATION:
	// return new TimePickerDialog(this, mTimeSetListenerDuration,
	// mHourthree, mMinutethree, true);
	// }
	// return null;
	// }

	private void updateDisplay() {
		int mMeasureHour = mHour;
		int mMeasureMinute = mMinute;
		if (mMeasureHour > 12) {
			mMeasureHour = mMeasureHour - 12;

			start_time.setText(new StringBuilder().append(pad(mMeasureHour))
					.append(":").append(pad(mMeasureMinute)).append("")
					.append(" PM"));

		} else if (mMeasureHour == 12) {
			start_time.setText(new StringBuilder().append(pad(mMeasureHour))
					.append(":").append(pad(mMeasureMinute)).append("")
					.append(" PM"));
		} else if (mHour == 0) {
			mMeasureHour = mHour + 12;
			start_time.setText(new StringBuilder().append(pad(mMeasureHour))
					.append(":").append(pad(mMeasureMinute)).append("")
					.append(" AM"));
		} else {
			start_time.setText(new StringBuilder().append(pad(mMeasureHour))
					.append(":").append(pad(mMeasureMinute)).append("")
					.append(" AM"));
		}

	}

	private void updateDisplaytwo() {
		int mMeasureHourTwo = mHourtwo;
		int mMeasureMinuteTwo = mMinutetwo;
		if (mMeasureHourTwo > 12) {
			mMeasureHourTwo = mMeasureHourTwo - 12;
			end_time.setText(new StringBuilder().append(pad(mMeasureHourTwo))
					.append(":").append(pad(mMeasureMinuteTwo)).append("")
					.append(" PM"));
		} else if (mMeasureHourTwo == 12) {
			end_time.setText(new StringBuilder().append(pad(mMeasureHourTwo))
					.append(":").append(pad(mMeasureMinuteTwo)).append("")
					.append(" PM"));
		} else if (mHourtwo == 0) {
			mMeasureHourTwo = mHourtwo + 12;
			end_time.setText(new StringBuilder().append(pad(mMeasureHourTwo))
					.append(":").append(pad(mMeasureMinuteTwo)).append("")
					.append(" AM"));
		} else {
			end_time.setText(new StringBuilder().append(pad(mMeasureHourTwo))
					.append(":").append(pad(mMeasureMinuteTwo)).append("")
					.append(" AM"));
		}

	}

	// private void updateDisplayDuration() {
	// int mMeasureHourTwo = mHourDuration;
	// int mMeasureMinuteTwo = mMinuteDuration;
	//
	// duration2.setText(new StringBuilder().append(pad(mMeasureHourTwo))
	// .append(":").append(pad(mMeasureMinuteTwo)));
	// }

	// private void updateDisplaythree() {
	// int mMeasureHourthree = mHourthree;
	// int mMeasureMinutethree = mMinutethree;
	// if (mMeasureHourthree > 12) {
	// mMeasureHourthree = mMeasureHourthree - 12;
	// duration2.setText(new StringBuilder()
	// .append(pad(mMeasureHourthree)).append(":").append(
	// pad(mMeasureMinutethree)).append(" ").append(""));
	// } else if (mMeasureHourthree == 12) {
	// duration2.setText(new StringBuilder()
	// .append(pad(mMeasureHourthree)).append(":").append(
	// pad(mMeasureMinutethree)).append("").append(""));
	// } else if (mHourthree == 0) {
	// mMeasureHourthree = mHourthree + 12;
	// duration2.setText(new StringBuilder()
	// .append(pad(mMeasureHourthree)).append(":").append(
	// pad(mMeasureMinutethree)).append("").append(""));
	// } else {
	// duration2.setText(new StringBuilder()
	// .append(pad(mMeasureHourthree)).append(":").append(
	// pad(mMeasureMinutethree)).append("").append(""));
	// }
	//
	// }

	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	protected void onDestroy() {

		super.onDestroy();
	}

	private boolean isOverlapping(MyContact myContact1, MyContact myContact2) {

		boolean isOverlapping = false;
		String startDays1;
		String endDays1;
		String startDays2;
		String endDays2;
		try {
			if (myContact1.getModeName().equalsIgnoreCase("General")) {
				startDays1 = "Su M Tu W Th F Sa";
				endDays1 = "Su M Tu W Th F Sa";
			} else {
				startDays1 = MainActivityTest.db.fetchModeByName(
						myContact1.getModeName()).getDay();
				endDays1 = getEndDaysString(myContact1);
			}

			if (myContact2.getModeName().equalsIgnoreCase("General")) {
				startDays2 = "Su M Tu W Th F Sa";
				endDays2 = "Su M Tu W Th F Sa";
			} else {
				startDays2 = MainActivityTest.db.fetchModeByName(
						myContact1.getModeName()).getDay();
				endDays2 = getEndDaysString(myContact1);
			}

			SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
			Date dateStart1 = null, dateEnd1 = null, dateStart2 = null, dateEnd2 = null;
			try {
				dateStart1 = dateFormat.parse(myContact1.getStartTime());
				dateEnd1 = dateFormat.parse(myContact1.getEndTime());
				dateStart2 = dateFormat.parse(myContact2.getStartTime());
				dateEnd2 = dateFormat.parse(myContact2.getEndTime());
				dateEnd1 = getEndDate(myContact1.getStartTime(),
						myContact1.getEndTime(), dateStart1, dateEnd1);
				dateEnd2 = getEndDate(myContact2.getStartTime(),
						myContact2.getEndTime(), dateStart2, dateEnd2);
			} catch (Exception e) {
				e.printStackTrace();
			}

			int[] startDaysArray1 = IOUtils
					.getIntArrayFromDayString(startDays1);
			int[] endDaysArray1 = IOUtils.getIntArrayFromDayString(endDays1);
			int[] startDaysArray2 = IOUtils
					.getIntArrayFromDayString(startDays2);
			int[] endDaysArray2 = IOUtils.getIntArrayFromDayString(endDays2);

			if (isDaysOverlapping(startDaysArray1, endDaysArray2)) {
				if (dateStart1.before(dateEnd2)
						&& !dateStart1.before(dateStart2)) {
					isOverlapping = true;
					return isOverlapping;
				} else {
					isOverlapping = false;
				}
			}

			if (isDaysOverlapping(startDaysArray2, endDaysArray1)) {
				if (dateStart2.before(dateEnd1)
						&& !dateStart2.before(dateStart1)) {
					isOverlapping = true;
					return isOverlapping;
				} else {
					isOverlapping = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return isOverlapping;
	}

	private boolean isDaysOverlapping(int[] array1, int[] array2) {
		boolean isOverlapping = false;

		for (int i = 0; i < array1.length; i++) {
			if (array1[i] == 1 && array2[i] == 1) {
				isOverlapping = true;
			}
		}

		return isOverlapping;
	}

	private String getEndDaysString(MyContact myContact) {

		String endDaysString = "";
		String startDaysString = "";
		try {

			if (myContact.getModeName().equalsIgnoreCase("General")) {
				startDaysString = "Su M Tu W Th F Sa";
			} else {
				startDaysString = MainActivityTest.db.fetchModeByName(
						myContact.getModeName()).getDay();
			}

			startDaysString = MainActivityTest.db.fetchModeByName(
					myContact.getModeName()).getDay();
			int[] startDaysArray;
			int[] endDaysArray;
			String startTtime = myContact.getStartTime();
			String endTime = myContact.getEndTime();
			String startZoneString = "";
			String endZoneString = "";
			boolean nextDay = false;
			SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
			Date dateStart = null, dateEnd = null;
			try {
				dateStart = dateFormat.parse(startTtime);
				dateEnd = dateFormat.parse(endTime);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (startTtime.contains("AM")) {
				startZoneString = "AM";
			} else {
				startZoneString = "PM";
			}

			if (endTime.contains("AM")) {
				endZoneString = "AM";
			} else {
				endZoneString = "PM";
			}

			startDaysArray = IOUtils.getIntArrayFromDayString(startDaysString);

			if (startZoneString.equalsIgnoreCase("AM")
					&& endZoneString.equalsIgnoreCase("AM")) {
				if (dateStart.before(dateEnd)) {
					nextDay = false;
				} else {
					nextDay = true;
				}
			} else if (startZoneString.equalsIgnoreCase("AM")
					&& endZoneString.equalsIgnoreCase("PM")) {
				nextDay = false;
			} else if (startZoneString.equalsIgnoreCase("PM")
					&& endZoneString.equalsIgnoreCase("AM")) {
				nextDay = true;
			} else if (startZoneString.equalsIgnoreCase("PM")
					&& endZoneString.equalsIgnoreCase("PM")) {
				if (dateStart.before(dateEnd)) {
					nextDay = false;
				} else {
					nextDay = true;
				}
			}

			if (nextDay) {
				int temp[] = new int[7];
				for (int i = 0; i < startDaysArray.length; i++) {
					if (startDaysArray[i] == 1) {
						if (i == 6) {
							temp[0] = 1;
						} else {
							temp[i + 1] = 1;
						}

					} else {
						if (i == 6) {
							temp[0] = 0;
							;
						} else {
							temp[i + 1] = 0;
							;
						}
					}
				}
				endDaysArray = temp;

				endDaysString = IOUtils.getDayStringFromIntArray(endDaysArray);
			} else {
				endDaysString = startDaysString;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return endDaysString;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

		int id = buttonView.getId();

		if (id == R.id.cb) {
			if (isChecked) {
				chkTime.setChecked(false);
				edtHours.setText("");
				edtMins.setText("");
				edtHours.setEnabled(true);
				edtMins.setEnabled(true);
				edtHours.setClickable(true);
				edtMins.setClickable(true);
				edtHours.setFocusable(true);
				edtMins.setFocusable(true);
				start_time.setText("");
				start_time.setEnabled(false);
				end_time.setText("");
				end_time.setEnabled(false);
			} else {
				chkTime.setChecked(true);
				edtHours.setText("");
				edtMins.setText("");
				edtHours.setEnabled(false);
				edtMins.setEnabled(false);
				edtHours.setClickable(false);
				edtMins.setClickable(false);
				edtHours.setFocusable(false);
				edtMins.setFocusable(false);
				start_time.setText("");
				start_time.setEnabled(true);
				end_time.setText("");
				end_time.setEnabled(true);
			}
		} else if (id == R.id.c2) {
			if (isChecked) {
				chkDuration.setChecked(false);
				start_time.setText("");
				start_time.setEnabled(true);
				end_time.setText("");
				end_time.setEnabled(true);
				edtHours.setText("");
				edtMins.setText("");
				edtHours.setEnabled(false);
				edtMins.setEnabled(false);
				edtHours.setClickable(false);
				edtMins.setClickable(false);
				edtHours.setFocusable(false);
				edtMins.setFocusable(false);
			} else {
				chkDuration.setChecked(true);
				start_time.setText("");
				start_time.setEnabled(false);
				end_time.setText("");
				end_time.setEnabled(false);
				edtHours.setText("");
				edtMins.setText("");
				edtHours.setEnabled(true);
				edtMins.setEnabled(true);
				edtHours.setClickable(true);
				edtMins.setClickable(true);
				edtHours.setFocusable(true);
				edtMins.setFocusable(true);
			}
		}

	}

	private Date getEndDate(String startTime, String endTime, Date dateStart,
			Date dateEnd) {
		Date realEndDate = null;
		String startZoneString = "";
		String endZoneString = "";
		boolean nextDay = false;

		try {
			if (startTime.contains("AM")) {
				startZoneString = "AM";
			} else {
				startZoneString = "PM";
			}

			if (endTime.contains("AM")) {
				endZoneString = "AM";
			} else {
				endZoneString = "PM";
			}

			if (startZoneString.equalsIgnoreCase("AM")
					&& endZoneString.equalsIgnoreCase("AM")) {
				if (dateStart.before(dateEnd)) {
					nextDay = false;
				} else {
					nextDay = true;
				}
			} else if (startZoneString.equalsIgnoreCase("AM")
					&& endZoneString.equalsIgnoreCase("PM")) {
				nextDay = false;
			} else if (startZoneString.equalsIgnoreCase("PM")
					&& endZoneString.equalsIgnoreCase("AM")) {
				nextDay = true;
			} else if (startZoneString.equalsIgnoreCase("PM")
					&& endZoneString.equalsIgnoreCase("PM")) {
				if (dateStart.before(dateEnd)) {
					nextDay = false;
				} else {
					nextDay = true;
				}
			}

			if (nextDay) {
				long difference, difference2;
				long _24Hours = 24 * 60 * 60 * 1000;

				long startTimeMillis = dateStart.getTime();
				long endTimeMillis = dateEnd.getTime();

				difference = endTimeMillis - startTimeMillis;

				if (difference < 0)
					difference = -difference;

				difference2 = _24Hours - difference;

				long newEndTimeMillis = startTimeMillis + difference2;
				realEndDate = new Date(newEndTimeMillis);
			} else {
				realEndDate = dateEnd;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return realEndDate;
	}

	private void overlapNotificationDialog() {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage(
						"You can not set overlapping time for the same contact.")
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {

								submit.setBackground(getResources()
										.getDrawable(R.drawable.btn));
							}
						}).show();

	}

	private void contactoutOfModeDialog(final boolean isOverlapping) {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage(
						"You are setting contact time which is not inside mode time settings.")
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								goAhead(isOverlapping);
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {

								submit.setBackground(getResources()
										.getDrawable(R.drawable.btn));
							}
						}).show();

	}

	private void deleteContactDialog() {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage("Do you really want to delete this contact?")
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								MainActivityTest.db.deleteContact(myContact
										.getId());

								Intent in = new Intent();
								Bundle b = new Bundle();
								String mode = spinnerMode.getSelectedItem()
										.toString();
								b.putString("MODE", mode);
								in.putExtras(b);
								setResult(RESULT_OK, in);
								finish();
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {

								btnDelete.setBackground(getResources()
										.getDrawable(R.drawable.btn));
							}
						}).show();

	}

	private void fieldBlankDialog() {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage("Fields can't be blank.")
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {

								submit.setBackground(getResources()
										.getDrawable(R.drawable.btn));
							}
						}).show();

	}

	private void selectNextDayDialog() {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage(
						"You cannot set end time for next day in general mode.")
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								submit.setBackground(getResources()
										.getDrawable(R.drawable.btn));

							}
						}).show();

	}

	private boolean goAhead(boolean isOverlapping) {

		if (!isDuration) {
			start = start_time.getText().toString();
			end = end_time.getText().toString();
		}

		if (isOverlapping) {
			overlapNotificationDialog();
		} else {
			if (myContact == null) {
				Log.e("hereeeeeeeee", "saveddddddddd");

				MainActivityTest.db.createContatcsRow(editContact.getText()
						.toString(), start, end, spmode.getSelectedItem()
						.toString(), spinnerMode.getSelectedItem().toString(),
						phoneNumber, "-1");

			} else {
				MainActivityTest.db.updateContactRow(myContact.getId(),
						editContact.getText().toString(), start, end, spmode
								.getSelectedItem().toString(), spinnerMode
								.getSelectedItem().toString(), myContact
								.getPhoneNumber());
			}
		}

		if (isContactInseretd) {
			Intent in = new Intent();
			Bundle b = new Bundle();
			String mode = spinnerMode.getSelectedItem().toString();
			startTime = (start);
			Log.i("STARTTIME.............", "" + startTime);
			endTime = (end);
			Log.i("ENDTIME.............", "" + endTime);
			b.putString("SelectContact", editContact.getText().toString());
			b.putString("MODE", mode);
			in.putExtras(b);
			setResult(RESULT_OK, in);

			// Intent it = new Intent(ContactSetting.this,
			// MainActivityTest.class);
			// startActivity(it);
			finish();
		}
		return (!isOverlapping);
	}

	// @Override
	// public void onBackPressed() {
	// // TODO Auto-generated method stubdateEnd1
	// super.onBackPressed();
	//
	// Intent it = new Intent(ContactSetting.this, Contactlist1.class);
	//
	// it.putExtra("currentMode", currentMode);
	// it.putExtra("start_time", startTimeExtra);
	// it.putExtra("end_time", endTimeExtra);
	// it.putExtra("ring_type", ringtypeExtra);
	// startActivity(it);
	// // finish();
	// }

	private boolean isContactoutOfMode(MyContact tempContact, Mode mode) {

		boolean isContactOut = false;

		try {

			if (!mode.getModeName().equalsIgnoreCase("General")) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
				Date dateStart1 = null, dateEnd1 = null, dateStart2 = null, dateEnd2 = null;
				try {
					dateStart1 = dateFormat.parse(tempContact.getStartTime());
					dateEnd1 = dateFormat.parse(tempContact.getEndTime());
					dateStart2 = dateFormat.parse(mode.getStartTime());
					dateEnd2 = dateFormat.parse(mode.getEndTime());
					dateEnd1 = getEndDate(tempContact.getStartTime(),
							tempContact.getEndTime(), dateStart1, dateEnd1);
					dateEnd2 = getEndDate(mode.getStartTime(),
							mode.getEndTime(), dateStart2, dateEnd2);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (dateStart1.before(dateStart2) || dateEnd1.after(dateEnd2)) {
					isContactOut = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return isContactOut;
	}

	private ArrayList<Date> getStart_EndTimeFromDuration(String duration) {

		ArrayList<Date> dateList = new ArrayList<Date>();
		Date startDate, endDate;

		try {
			Calendar cal = Calendar.getInstance();
			startDate = cal.getTime();

			String[] temp = duration.split(":");
			int hour = Integer.parseInt(temp[0].trim());
			int minute = Integer.parseInt(temp[1].trim());

			endDate = new Date(startDate.getTime() + (hour * 60 * 60 * 1000)
					+ (minute * 60 * 1000));

			dateList.add(startDate);
			dateList.add(endDate);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return dateList;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == 11) {
				start_time.setText(data.getExtras().getString("hours") + ":"
						+ data.getExtras().getString("mins") + " "
						+ data.getExtras().getString("ampm"));
			} else if (requestCode == 12) {
				end_time.setText(data.getExtras().getString("hours") + ":"
						+ data.getExtras().getString("mins") + " "
						+ data.getExtras().getString("ampm"));
			} else if (requestCode == 13) {
				if (data.getExtras().getString("hours").equalsIgnoreCase("0")
						|| data.getExtras().getString("hours")
								.equalsIgnoreCase("1")) {
					edtHours.setText(data.getExtras().getString("hours")
							+ " hour");
				} else {
					edtHours.setText(data.getExtras().getString("hours")
							+ " hours");
				}

			} else if (requestCode == 14) {
				if (data.getExtras().getString("mins").equalsIgnoreCase("0")
						|| data.getExtras().getString("mins")
								.equalsIgnoreCase("1")) {
					edtMins.setText(data.getExtras().getString("mins")
							+ " minute");
				} else {
					edtMins.setText(data.getExtras().getString("mins")
							+ " minutes");
				}
			}
		}
	}

	private class SpinnerAdapter_ extends ArrayAdapter<String> {
		Context context;
		// String[] items = new String[] {};
		ArrayList<String> arrayList;

		public SpinnerAdapter_(final Context context,
				final int textViewResourceId, ArrayList<String> arrayListName) {
			super(context, textViewResourceId, arrayListName);
			this.arrayList = arrayListName;
			this.context = context;
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.spinner_dropdown,
						parent, false);
			}

			TextView tv = (TextView) convertView.findViewById(R.id.vibrate);
			tv.setText(arrayList.get(position));
			// tv.setTextColor(Color.BLACK);
			tv.setTextSize(15);
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.spinner_layout, parent,
						false);
			}

			TextView tv = (TextView) convertView.findViewById(R.id.spinner);
			tv.setText(arrayList.get(position));
			// tv.setTextColor(Color.BLACK);
			// tv.setHint("City");

			tv.setTextSize(15);

			if (position == 0) {
				// tv.setTextColor(Color.GRAY);
			}
			return convertView;
		}

	}

	private class SpinnerModeAdapter extends ArrayAdapter<String> {
		Context context;
		// String[] items = new String[] {};
		String[] arrayList;

		public SpinnerModeAdapter(final Context context,
				final int textViewResourceId, String[] array_spinner) {
			super(context, textViewResourceId);
			this.arrayList = array_spinner;
			this.context = context;
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.spinner_dropdown,
						parent, false);
			}

			TextView tv = (TextView) convertView.findViewById(R.id.vibrate);
			tv.setText(arrayList.length);
			// tv.setTextColor(Color.BLACK);
			tv.setTextSize(15);
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.spinner_layout, parent,
						false);
			}

			TextView tv = (TextView) convertView.findViewById(R.id.spinner);
			tv.setText(arrayList.length);
			// tv.setTextColor(Color.BLACK);
			// tv.setHint("City");

			tv.setTextSize(15);

			if (position == 0) {
				// tv.setTextColor(Color.GRAY);
			}
			return convertView;
		}

	}
}
