package pro.com.data;

public class Mode {

	private int id;
	private String modeName;
	private String startTime;
	private String endTime;
	private String day;
	private String ringerOption;
	private int modeNumber;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getModeName() {
		return modeName;
	}

	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getRingerOption() {
		return ringerOption;
	}

	public void setRingerOption(String ringerOption) {
		this.ringerOption = ringerOption;
	}

	public int getModeNumber() {
		return modeNumber;
	}

	public void setModeNumber(int modeNumber) {
		this.modeNumber = modeNumber;
	}
}
