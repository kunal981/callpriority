package pro.com;

import java.io.ObjectOutputStream.PutField;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import pro.com.data.ConstantData;
import pro.com.data.IOUtils;
import pro.com.data.Mode;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

public class ModeSetting11 extends Activity {
	Spinner sp;
	String id;
	Long mRowId;
	Button submit;
	Spinner spmode, spinnermode;
	String[] name;
	EditText name1, start_time, end_time;
	private Button btnDelete;
	private int mHour;
	private int mMinute;
	private int mDay, mMonth, mYear;
	String phonenumber;
	CheckBox checkBox, cb;
	CheckBox checkBox2;
	int starttimeedit = 0;
	int endtimeedit = 0;
	String act2MyValue, modeNameString;
	String dayString = null;
	static final int TIME_DIALOG_ID_ONE = 0;
	static final int TIME_DIALOG_ID_TWO = 1;
	static final int TIME_DIALOG_ID_THREE = 2;
	static final int DIALOG_ID_ERROR = 3;
	static final int DIALOG_ID_TIME = 4;
	int i = 0;
	EditText modeName;
	EditText mode;
	String startTime, endTime;
	EditText vibratemode;
	ContactsDatabaseHelper contactsdbHelper;
	static final int DATE_DIALOG_ID = 0;
	static final int START_TIME_DIALOG_ID = 1;
	static final int END_TIME_DIALOG_ID = 2;
	private int mode_no;
	android.app.AlertDialog.Builder dialog = null;
	private Mode modeObject = null;
	private int[] days;
	private int[] checkBoxValues = { 0, 0, 0, 0, 0, 0, 0 };
	private CheckBox[] checkBoxArray = new CheckBox[7];
	private final String PREFS_NAME = "CALL_PRIORITY";

	ArrayList<String> arrayListName;

	boolean flag;
	int j;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		if (ConstantData.mode != null) {
			modeObject = ConstantData.mode;
			ConstantData.mode = null;
		}

		if (modeObject != null) {
			setContentView(R.layout.modesetting_edit);
			btnDelete = (Button) findViewById(R.id.Button02);
			btnDelete.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					btnDelete.setBackground(getResources().getDrawable(
							R.drawable.btn_slct));
					deleteModeDialog();
				}
			});
		} else {

			setContentView(R.layout.modesetting_new);
		}

		Bundle bundle = getIntent().getExtras();
		mode_no = bundle.getInt("MODE_NO");
		Log.e("mode_no", "" + mode_no);
		modeName = (EditText) findViewById(R.id.et1);
		start_time = (EditText) findViewById(R.id.start_time);
		end_time = (EditText) findViewById(R.id.end_time);
		checkBox = (CheckBox) findViewById(R.id.cb);
		checkBox2 = (CheckBox) findViewById(R.id.c2);
		checkBoxArray[0] = (CheckBox) findViewById(R.id.checkBox1);
		checkBoxArray[1] = (CheckBox) findViewById(R.id.checkBox2);
		checkBoxArray[2] = (CheckBox) findViewById(R.id.checkBox3);
		checkBoxArray[3] = (CheckBox) findViewById(R.id.checkBox4);
		checkBoxArray[4] = (CheckBox) findViewById(R.id.checkBox5);
		checkBoxArray[5] = (CheckBox) findViewById(R.id.checkBox6);
		checkBoxArray[6] = (CheckBox) findViewById(R.id.checkBox7);

		setDayCheckBoxes();

		arrayListName = new ArrayList<String>();
		arrayListName.add("Vibrate");
		arrayListName.add("Ring");
		contactsdbHelper = new ContactsDatabaseHelper(this);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.items2, android.R.layout.simple_spinner_item);

		spmode = (Spinner) findViewById(R.id.viratering);

		// adapter = ArrayAdapter.createFromResource(this, R.array.items1,
		// android.R.layout.simple_spinner_item);

		adapter = ArrayAdapter.createFromResource(this, R.array.items1,
				R.layout.spinner_mode);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// spmode.setAdapter(new MyAdapter(this, R.layout.spinner_layout,
		// R.array.items1));

		// SpinnerAdapter_ sadapter = new
		// SpinnerAdapter_(getApplicationContext(),
		// R.layout.spinner_layout, arrayListName);
		// Log.e("arraylist", ""+arrayListName);
		// spmode.setAdapter(sadapter);

		spmode.setAdapter(adapter);

		final Calendar c = Calendar.getInstance();
		mHour = c.get(Calendar.HOUR_OF_DAY);
		mMinute = c.get(Calendar.MINUTE);

		start_time.setFocusable(false);

		start_time.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// showDialog(START_TIME_DIALOG_ID);
				Intent intent = new Intent(ModeSetting11.this,
						TimePickerActivity.class);

				// Log.e("stime",modeObject.getStartTime());
				try {
					intent.putExtra("time", modeObject.getStartTime());
				} catch (Exception e) {
					intent.putExtra("time", "none");
				}

				startActivityForResult(intent, 11);
			}
		});

		end_time.setFocusable(false);

		// get the current date
		final Calendar cl = Calendar.getInstance();
		mYear = cl.get(Calendar.YEAR);
		mMonth = cl.get(Calendar.MONTH);
		mDay = cl.get(Calendar.DAY_OF_MONTH);

		// day.setOnClickListener(new View.OnClickListener() {
		// public void onClick(View v) {
		// Intent intent = new
		// Intent(ModeSetting11.this,SelectDayActivity.class);
		// intent.putExtra("checkBoxValues",
		// IOUtils.getIntArrayFromDayString(day.getText().toString()));
		// startActivityForResult(intent,1);
		// }
		// });

		start_time.setFocusable(false);
		submit = (Button) findViewById(R.id.Button01);

		submit.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {

				submit.setBackground(getResources().getDrawable(
						R.drawable.btn_slct));

				String name = modeName.getText().toString();
				ArrayList<Mode> modeList = MainActivityTest.db.fetchModeList();
				boolean isNameCopy = false;

				for (int i = 0; i < modeList.size(); i++) {

					if (modeObject != null) {
						if (modeObject.getId() == modeList.get(i).getId()
								&& modeList.get(i).getModeName()
										.equalsIgnoreCase(name)) {
							isNameCopy = false;
						} else if (modeObject.getId() != modeList.get(i)
								.getId()
								&& modeList.get(i).getModeName()
										.equalsIgnoreCase(name)) {
							isNameCopy = true;
							break;
						}
					} else {
						if (modeList.get(i).getModeName()
								.equalsIgnoreCase(name)) {
							isNameCopy = true;
							break;
						}
					}
				}

				if (isNameCopy) {
					nameCopyDialog();
				} else {
					for (int i = 0; i < checkBoxArray.length; i++) {
						if (checkBoxArray[i].isChecked()) {
							checkBoxValues[i] = 1;

						} else {

							checkBoxValues[i] = 0;

						}
					}

					dayString = IOUtils
							.getDayStringFromIntArray(checkBoxValues);

					if (name.trim().equalsIgnoreCase("")) {
						nameProperDialog();
					} else {
						if (modeName.getText().toString().equalsIgnoreCase("")
								|| start_time.getText().toString()
										.equalsIgnoreCase("")
								|| end_time.getText().toString()
										.equalsIgnoreCase("")
								|| dayString.equalsIgnoreCase("")) {
							fieldBlankDialog();
						} else {

							String SRTTIME = start_time.getText().toString();
							String ENDTIME = end_time.getText().toString();
							// Log.e("SRTTIME", SRTTIME);
							// Log.e("ENDTIME", ENDTIME);
							Log.e("dayString", "" + dayString + ","
									+ checkBoxValues[i]);

							for (int i = 0; i < checkBoxValues.length; i++) {

								if (checkBoxArray[i].isChecked()) {
									j++;
								}
							}

							if (SRTTIME.contains("PM")
									&& ENDTIME.contains("AM") && j <= 1) {

								selectNextDayDialog();
								Log.e("hereeeeee", "hereeeeeee");
							} else {
								insertMode();
								Intent i = new Intent();
								i.putExtra("isDelete", false);
								setResult(RESULT_OK, i);
								finish();
							}
						}
					}
				}

			}

		});

		end_time.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// showDialog(END_TIME_DIALOG_ID);
				Intent intent = new Intent(ModeSetting11.this,
						TimePickerActivity.class);

				// Log.e("etime",modeObject.getEndTime());

				try {
					intent.putExtra("time", modeObject.getEndTime());
				} catch (Exception e) {
					intent.putExtra("time", "none");
				}

				startActivityForResult(intent, 12);
			}
		});

		if (modeObject != null) {
			setModeValues();
		}

	}

	private void setDayCheckBoxes() {

		if (modeObject != null) {
			String dayString = modeObject.getDay();
			checkBoxValues = IOUtils.getIntArrayFromDayString(dayString);
			Log.e("dayString", "" + dayString);
		}
		for (int i = 0; i < checkBoxValues.length; i++) {
			if (checkBoxValues[i] == 1) {
				checkBoxArray[i].setChecked(true);

				// if (checkBoxArray[i].isChecked()) {
				// Log.e("ifffffff", "ifffffff");
				// } else {
				// Log.e("elseeeeeeee", "elseeeeee");
				// }

			}
		}
	}

	public void insertMode() {

		for (int i = 0; i < checkBoxArray.length; i++) {
			if (checkBoxArray[i].isChecked()) {
				checkBoxValues[i] = 1;

			} else {
				checkBoxValues[i] = 0;

			}
		}

		String dayString = IOUtils.getDayStringFromIntArray(checkBoxValues);
		if (modeObject != null) {
			MainActivityTest.db.updateModesRow(modeObject.getId(), modeName
					.getText().toString(), start_time.getText().toString(),
					end_time.getText().toString(), dayString, spmode
							.getSelectedItem().toString(), mode_no);
			MainActivityTest.db.updateAllContactsByMode(modeObject
					.getModeName(), start_time.getText().toString(), end_time
					.getText().toString(), spmode.getSelectedItem().toString());

			if (!modeObject.getModeName().equals(modeName.getText().toString())) {
				MainActivityTest.db
						.updateAllContactsForModeNameChange(modeObject
								.getModeName(), modeName.getText().toString());
			}
		} else {
			MainActivityTest.db.createModesRow(modeName.getText().toString(),
					start_time.getText().toString(), end_time.getText()
							.toString(), dayString, spmode.getSelectedItem()
							.toString(), mode_no);
		}
	}

	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// return new DatePickerDialog(this,
			// mDateSetListener,
			// mYear, mMonth, mDay);

		case START_TIME_DIALOG_ID:
			return new TimePickerDialog(this, mStartTimeSetListener, mHour,
					mMinute, false);

		case END_TIME_DIALOG_ID:
			return new TimePickerDialog(this, mEndTimeSetListener, mHour,
					mMinute, false);

		}
		return null;
	}

	// // updates the date we display in the TextView
	// private void updateDisplay() {
	// day.setText(
	// new StringBuilder()
	// // Month is 0 based so add 1
	// .append(mMonth + 1).append("-")
	// .append(mDay).append("-")
	// .append(mYear).append(" "));
	// }

	private void updateStartTimeDisplay(String timeZone) {
		start_time.setText(new StringBuilder().append(pad(mHour)).append(":")
				.append(pad(mMinute)).append(" ").append(timeZone));

	}

	private void updateEndTimeDisplay(String timeZone) {
		end_time.setText(new StringBuilder().append(pad(mHour)).append(":")
				.append(pad(mMinute)).append(" ").append(timeZone));

	}

	// // the callback received when the user "sets" the date in the dialog
	// private DatePickerDialog.OnDateSetListener mDateSetListener =
	// new DatePickerDialog.OnDateSetListener() {
	//
	// public void onDateSet(DatePicker view, int year,
	// int monthOfYear, int dayOfMonth) {
	// mYear = year;
	// mMonth = monthOfYear;
	// mDay = dayOfMonth;
	// updateDisplay();
	// }
	// };

	// the callback received when the user "sets" the time in the dialog
	private TimePickerDialog.OnTimeSetListener mStartTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			String timeZone = "";
			if (hourOfDay > 12) {
				mHour = hourOfDay - 12;
				timeZone = "PM";
			} else if (hourOfDay == 12) {
				mHour = hourOfDay;
				timeZone = "PM";
			} else if (hourOfDay == 0) {
				mHour = hourOfDay + 12;
				timeZone = "AM";
			} else if (hourOfDay < 12) {
				mHour = hourOfDay;
				timeZone = "AM";
			}
			mMinute = minute;
			updateStartTimeDisplay(timeZone);
		}
	};

	private TimePickerDialog.OnTimeSetListener mEndTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			String timeZone = "";
			if (hourOfDay > 12) {
				mHour = hourOfDay - 12;
				timeZone = "PM";
			} else if (hourOfDay == 12) {
				mHour = hourOfDay;
				timeZone = "PM";
			} else if (hourOfDay == 0) {
				mHour = hourOfDay + 12;
				timeZone = "AM";
			} else if (hourOfDay < 12) {
				mHour = hourOfDay;
				timeZone = "AM";
			}
			mMinute = minute;
			updateEndTimeDisplay(timeZone);
		}
	};

	private void setModeValues() {
		modeName.setText(modeObject.getModeName());
		modeName.setSelection(modeName.getText().length());
		start_time.setText(modeObject.getStartTime());
		// Log.e("start_time", ""+modeObject.getStartTime());

		end_time.setText(modeObject.getEndTime());
		// Log.e("end_time", ""+modeObject.getEndTime());

		if (modeObject.getRingerOption().equalsIgnoreCase("Vibrate")) {
			spmode.setSelection(0);
		} else {
			spmode.setSelection(1);
		}
	}

	private void deleteMode() {

		// AudioManager amanager;
		//
		// amanager = (AudioManager) getBaseContext().getSystemService(
		// AUDIO_SERVICE);
		//
		// amanager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
		//
		// amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);

		MainActivityTest.db.deleteMode(modeObject.getId());
		MainActivityTest.db.deleteContactByMode(modeObject.getModeName());
		SharedPreferences shPreferences = getSharedPreferences(PREFS_NAME,
				Context.MODE_WORLD_WRITEABLE);
		SharedPreferences.Editor editor;
		editor = shPreferences.edit();
		editor.putInt("MODE_NO", 0);
		editor.commit();

		Intent intent = new Intent(ModeSetting11.this, MainActivityTest.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
		Intent i = new Intent();
		i.putExtra("isDelete", true);
		setResult(RESULT_OK, i);
		finish();
	}

	private boolean performLocalCheck(String startTime, String endTime) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");

		if (startTime.contains("AM") && endTime.contains("AM")) {
			try {
				Date dateStart = dateFormat.parse(startTime);
				Date dateEnd = dateFormat.parse(endTime);
				return dateStart.before(dateEnd);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		if (startTime.contains("AM") && endTime.contains("PM")) {
			try {
				Date dateStart = dateFormat.parse(startTime);
				Date dateEnd = dateFormat.parse(endTime);
				return dateStart.before(dateEnd);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	private void fieldBlankDialog() {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage("Fields can't be blank.")
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								submit.setBackground(getResources()
										.getDrawable(R.drawable.btn));
							}
						}).show();

	}

	private void selectNextDayDialog() {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage(
						"Please Select the next day of the week to allow AM end time.")
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								submit.setBackground(getResources()
										.getDrawable(R.drawable.btn));

							}
						}).show();

	}

	private void nameProperDialog() {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage("Please give proper name for the mode.")
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								submit.setBackground(getResources()
										.getDrawable(R.drawable.btn));
							}
						}).show();

	}

	private void nameCopyDialog() {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage(
						"There is already a mode with this name.Please choose a different name.")
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								submit.setBackground(getResources()
										.getDrawable(R.drawable.btn));
							}
						}).show();

	}

	private void deleteModeDialog() {
		new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage("Do you really want to delete this mode?")
				.setPositiveButton(R.string.str_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								deleteMode();
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {

							}
						}).show();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == 11) {
				start_time.setText(data.getExtras().getString("hours") + ":"
						+ data.getExtras().getString("mins") + " "
						+ data.getExtras().getString("ampm"));
			} else if (requestCode == 12) {
				end_time.setText(data.getExtras().getString("hours") + ":"
						+ data.getExtras().getString("mins") + " "
						+ data.getExtras().getString("ampm"));
			}
		}

	}

	private class SpinnerAdapter_ extends ArrayAdapter<String> {
		Context context;
		// String[] items = new String[] {};
		ArrayList<String> arrayList;

		public SpinnerAdapter_(final Context context,
				final int textViewResourceId, ArrayList<String> arrayListName) {
			super(context, textViewResourceId, arrayListName);
			this.arrayList = arrayListName;
			this.context = context;
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.spinner_dropdown,
						parent, false);
			}

			TextView tv = (TextView) convertView.findViewById(R.id.vibrate);
			tv.setText(arrayList.get(position));
			// tv.setTextColor(Color.BLACK);
			tv.setTextSize(15);
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.spinner_layout, parent,
						false);
			}

			TextView tv = (TextView) convertView.findViewById(R.id.spinner);
			tv.setText(arrayList.get(position));
			// tv.setTextColor(Color.BLACK);
			// tv.setHint("City");

			tv.setTextSize(15);

			if (position == 0) {
				// tv.setTextColor(Color.GRAY);
			}
			return convertView;
		}

	}

	protected void onDestroy() {
		contactsdbHelper.close();
		super.onDestroy();
	}
}
