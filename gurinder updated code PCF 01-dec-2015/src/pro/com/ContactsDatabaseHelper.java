package pro.com;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

public class ContactsDatabaseHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "callpriority.db";
	public static final int DATABASE_VERSION = 1;
	public static final String KEY_NAME = "name";

	public static final String TABLE1 = "mode1";
	public static final String TABLE2 = "mode2";
	public static final String TABLE3 = "mode3";
	public static final String TABLE4 = "mode4";
	public static final String TABLE5 = "home";

	/* Adding columns */
	public static final String NAME = "name";
	public static final String STARTTIME = "starttime";
	public static final String ENDTIME = "endtime";
	public static final String DURATION = "duration";
	public static final String PHONEOPTION = "phoneoption";
	public static final String MODE = "mode";

	public static final String KEY_PHONENUMBER = "phonenumber";
	public static final String KEY_SUNDAY = "sunday";
	public static final String KEY_MONDAY = "monday";
	public static final String KEY_TUESDAY = "tueday";
	public static final String KEY_WEDDAY = "wedday";
	public static final String KEY_THRDAY = "thuday";
	public static final String KEY_FRIDAY = "friday";
	public static final String KEY_SATDAY = "satday";
	public static final String KEY_EVERYDAY = "everyday";

	public static final String KEY_ROWID = "_id";
	public static final String KEY_DAYS = "days";

	public ContactsDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String mode1 = "create table " + TABLE1 + "( " + BaseColumns._ID
				+ " integer PRIMARY KEY AUTOINCREMENT," + NAME + " text,"
				+ STARTTIME + " text," + ENDTIME + " text," + DURATION
				+ " text," + PHONEOPTION + " text," + MODE + " text,"
				+ "sunday integer, monday integer," + KEY_TUESDAY + " integer,"
				+ KEY_WEDDAY + " integer," + KEY_THRDAY + " integer,"
				+ KEY_FRIDAY + " integer," + KEY_SATDAY + " integer,"
				+ KEY_EVERYDAY + " integer);";

		String mode2 = "create table " + TABLE2 + "( " + BaseColumns._ID
				+ " integer PRIMARY KEY AUTOINCREMENT," + NAME + " text,"
				+ STARTTIME + " text," + ENDTIME + " text," + DURATION
				+ " text," + PHONEOPTION + " text," + MODE + " text,"
				+ "sunday integer, monday integer," + KEY_TUESDAY + " integer,"
				+ KEY_WEDDAY + " integer," + KEY_THRDAY + " integer,"
				+ KEY_FRIDAY + " integer," + KEY_SATDAY + " integer,"
				+ KEY_EVERYDAY + " integer);";

		String mode3 = "create table " + TABLE3 + "( " + BaseColumns._ID
				+ " integer PRIMARY KEY AUTOINCREMENT," + NAME + " text,"
				+ STARTTIME + " text," + ENDTIME + " text," + DURATION
				+ " text," + PHONEOPTION + " text," + MODE + " text,"
				+ "sunday integer, monday integer," + KEY_TUESDAY + " integer,"
				+ KEY_WEDDAY + " integer," + KEY_THRDAY + " integer,"
				+ KEY_FRIDAY + " integer," + KEY_SATDAY + " integer,"
				+ KEY_EVERYDAY + " integer);";

		String mode4 = "create table " + TABLE4 + "( " + BaseColumns._ID
				+ " integer PRIMARY KEY AUTOINCREMENT," + NAME + " text,"
				+ STARTTIME + " text," + ENDTIME + " text," + DURATION
				+ " text," + PHONEOPTION + " text," + MODE + " text,"
				+ "sunday integer, monday integer," + KEY_TUESDAY + " integer,"
				+ KEY_WEDDAY + " integer," + KEY_THRDAY + " integer,"
				+ KEY_FRIDAY + " integer," + KEY_SATDAY + " integer,"
				+ KEY_EVERYDAY + " integer);";

		String home = "create table " + TABLE5 + "( " + BaseColumns._ID
				+ " integer PRIMARY KEY AUTOINCREMENT," + NAME + " text,"
				+ STARTTIME + " text," + ENDTIME + " text," + DURATION
				+ " text," + PHONEOPTION + " text," + MODE + " text,"
				+ "sunday integer, monday integer," + KEY_TUESDAY + " integer,"
				+ KEY_WEDDAY + " integer," + KEY_THRDAY + " integer,"
				+ KEY_FRIDAY + " integer," + KEY_SATDAY + " integer,"
				+ KEY_EVERYDAY + " integer);";

		db.execSQL(mode1);
		db.execSQL(mode2);
		db.execSQL(mode3);
		db.execSQL(mode4);
		db.execSQL(home);

		Log.i("DATABASE ", "db created ");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE IF EXIST model");
		db.execSQL("DROP TABLE IF EXIST mode2");
		db.execSQL("DROP TABLE IF EXIST mode3");
		db.execSQL("DROP TABLE IF EXIST mode4");
		db.execSQL("DROP TABLE IF EXIST home");
		onCreate(db);

	}

}
