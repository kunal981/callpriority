package pro.com;

import java.util.Calendar;
import java.util.Locale;

import pro.com.data.IOUtils;
import pro.com.wheelpicker.ArrayWheelAdapter;
import pro.com.wheelpicker.NumericWheelAdapter;
import pro.com.wheelpicker.WheelView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MinutesPickerActivity extends Activity {

	private Button btnOk, btnCancel;
	private WheelView mins;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.min_picker);

		btnOk = (Button) findViewById(R.id.btnOk);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				String minssString = IOUtils.getTimeString(mins
						.getCurrentItem());
				intent.putExtra("mins", minssString);
				setResult(RESULT_OK, intent);
				finish();
			}
		});

		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		mins = (WheelView) findViewById(R.id.mins);
		NumericWheelAdapter minAdapter = new NumericWheelAdapter(this, 0, 59,
				"%02d");
		minAdapter.setItemResource(R.layout.wheel_text_item);
		minAdapter.setItemTextResource(R.id.text);
		mins.setViewAdapter(minAdapter);
		mins.setCyclic(true);

		// set current time
		Calendar calendar = Calendar.getInstance(Locale.US);
		mins.setCurrentItem(calendar.get(Calendar.MINUTE));

	}
}
