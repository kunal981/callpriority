package pro.com;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.GroupMembership;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

public class GroupsContacts extends Activity {
	EditText editsearch;
	private String currentMode;
	Button btn_contacts, btngroup;

	private String phoneNumber;
	private String id[];
	private ArrayList<Integer> sid;

	private String startTimeExtra, endTimeExtra, ringTypeExtra;

	ListView mGroupContactList;
	GroupAdapter groupAdapter;
	ArrayList<String> groupid;
	ArrayList<String> tittle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.group_contacts);
		sid = new ArrayList<Integer>();
		editsearch = (EditText) findViewById(R.id.editsearch);
		btn_contacts = (Button) findViewById(R.id.btn_contacts);
		currentMode = getIntent().getExtras().getString("currentMode");

		// Log.e("currentModeContactList", "" + currentMode);
		btngroup = (Button) findViewById(R.id.btn_groups);
		startTimeExtra = getIntent().getExtras().getString("start_time");
		endTimeExtra = getIntent().getExtras().getString("end_time");
		ringTypeExtra = getIntent().getExtras().getString("ring_type");

		// Log.e("currentModeGroup", "" + currentMode);

		mGroupContactList = (ListView) findViewById(R.id.ListView01);

		groupid = new ArrayList<String>();
		tittle = new ArrayList<String>();

		groupAdapter = new GroupAdapter(getApplicationContext(),
				getLayoutInflater(), tittle);

		populateGroupContactList();

		// populateGroupContactList1();

		btngroup.setBackground(getResources().getDrawable(R.drawable.btn_slc1t));
		btn_contacts.setBackground(getResources().getDrawable(R.drawable.btn));
		btngroup.setPadding(0, 0, 0, 18);

		mGroupContactList.setAdapter(groupAdapter);
		mGroupContactList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// // TODO Auto-generated method stub
				// Log.e("ContactsContract.Groups._ID", ""
				// + ContactsContract.Groups._ID);
				// Log.e("", ""
				// + ContactsContract.CommonDataKinds.Phone.CONTENT_URI);

				Intent it = new Intent(GroupsContacts.this,
						GroupContactSetting.class);

				String strgroup = (String) mGroupContactList
						.getItemAtPosition(position);

				String selectedgroupid;

				if (sid.size() < groupid.size() && sid.size() > 0) {
					selectedgroupid = (String) groupid.get(sid.get(position));
				} else {
					selectedgroupid = (String) groupid.get(position);
				}

				Bundle bdl1 = new Bundle();
				bdl1.putString("strgroup", strgroup);
				bdl1.putString("currentMode", currentMode);

				bdl1.putString("selectedgroupid", selectedgroupid);

				bdl1.putString("phoneNumber", phoneNumber);
				bdl1.putString("start_time", startTimeExtra);
				bdl1.putString("end_time", endTimeExtra);
				bdl1.putString("ring_type", ringTypeExtra);
				it.putExtras(bdl1);
				startActivityForResult(it, 1);
				// startActivity(it);
				finish();

			}
		});

		editsearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				// String text =
				// editsearch.getText().toString().toLowerCase(Locale.getDefault());
				//
				// contactAdapter.filter(text);
				groupAdapter.getFilter().filter(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		btn_contacts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				btn_contacts.setBackground(getResources().getDrawable(
						R.drawable.btn_slc1t));
				// btn_contacts.setPadding(0, 0, 0, 22);
				// btngroup.setBackground(getResources().getDrawable(
				// R.drawable.btn));
				finish();
			}
		});

	}

	private void populateGroupContactList() {
		// Build adapter with contact entries

		final String[] GROUP_PROJECTION = new String[] {
				ContactsContract.Groups._ID, ContactsContract.Groups.TITLE };

		Cursor cursor = getContentResolver().query(
				ContactsContract.Groups.CONTENT_URI, GROUP_PROJECTION, null,
				null, ContactsContract.Groups.TITLE);

		int i = 0;

		// id = new String[cursor.getCount()];

		// String group[] = new String[cursor.getCount()];

		if (cursor.getCount() > 0) {
			while (cursor.moveToNext()) {

				int noOfContacts = getNumberOfContacts(cursor.getString(cursor
						.getColumnIndex(ContactsContract.Groups._ID)));

				// Log.e("noOfContacts", "" + noOfContacts);

				if (noOfContacts > 0) {
					groupid.add(cursor.getString(cursor
							.getColumnIndex(ContactsContract.Groups._ID)));

					// Log.e("groupid", "" + groupid);

					tittle.add(cursor.getString(cursor
							.getColumnIndex(ContactsContract.Groups.TITLE)));

					// id[i] = cursor.getString(cursor
					// .getColumnIndex(ContactsContract.CommonDataKinds.GroupMembership._ID));
					//

					// Log.e("id", "" + id);
					//
					// group[i] = cursor
					// .getString(cursor
					// .getColumnIndex(ContactsContract.Groups.TITLE));

					// i++;

				}
			}
		}

	}

	public int getNumberOfContacts(String groupid) {
		long groupId = Integer.parseInt(groupid);

		// Log.e("gid:", "" + groupId);

		String[] cProjection = { Contacts.DISPLAY_NAME,
				GroupMembership.CONTACT_ID };

		Cursor groupCursor = getContentResolver()
				.query(Data.CONTENT_URI,
						cProjection,
						CommonDataKinds.GroupMembership.GROUP_ROW_ID
								+ "= ?"
								+ " AND "
								+ ContactsContract.CommonDataKinds.GroupMembership.MIMETYPE
								+ "='"
								+ ContactsContract.CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE
								+ "'",
						new String[] { String.valueOf(groupId) }, null);

		int i = groupCursor.getCount();

		Log.e("numberscon:", "" + groupCursor.getCount());

		groupCursor.close();

		return i;
	}

	public void onBackPressed() {
		finish();
	};

	class GroupAdapter extends BaseAdapter {
		Context mContext;
		LayoutInflater inflater;
		ArrayList<String> mArrayList = new ArrayList<String>();
		private ArrayList<String> originalData = null;
		private ItemFilter mFilter = new ItemFilter();

		public GroupAdapter(Context mContext, LayoutInflater inflater,
				ArrayList<String> ArrayList) {
			super();
			this.mContext = mContext;
			this.inflater = inflater;
			this.mArrayList = ArrayList;
			originalData = ArrayList;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mArrayList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mArrayList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			final ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();

				convertView = inflater
						.inflate(R.layout.contactlist_items, null);

				holder.textview = (TextView) convertView
						.findViewById(R.id.text1);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.textview.setText(mArrayList.get(position));
			return convertView;
		}

		public class ViewHolder {

			TextView textview;
		}

		public Filter getFilter() {
			// TODO Auto-generated method stub
			return mFilter;
		}

		private class ItemFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {

				String filterString = constraint.toString().toLowerCase();

				FilterResults results = new FilterResults();

				final List<String> list = originalData;

				int count = list.size();
				final ArrayList<String> nlist = new ArrayList<String>(count);

				sid = new ArrayList<Integer>(count);

				String filterableString;

				for (int i = 0; i < count; i++) {
					filterableString = list.get(i);
					if (filterableString.toLowerCase().contains(filterString)) {
						nlist.add(filterableString);
						sid.add(i);
					}
				}

				results.values = nlist;
				results.count = nlist.size();

				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				mArrayList = (ArrayList<String>) results.values;
				Log.e("", "" + mArrayList);
				notifyDataSetChanged();
			}

		}
	}
}
