package pro.com.data;

import pro.com.R;
import android.content.Context;
import android.graphics.Color;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MyTextView {

	private FrameLayout frameLayout;
	private TextView textView;

	public MyTextView(Context context, FrameLayout frameLayout,
			TextView textView) {
		this.frameLayout = frameLayout;
		this.textView = textView;
		// if (textView.getText().toString().equalsIgnoreCase("")) {
		// this.frameLayout.setBackgroundResource(R.drawable.blank);
		// } else {
		// this.frameLayout.setBackgroundResource(R.drawable.blank);
		// // this.frameLayout.setBackgroundResource(R.drawable.blankyellow);
		// }
	}

	public void setText(CharSequence text) {
		textView.setText(text);

		if (textView.getText().toString().equalsIgnoreCase("")) {
			textView.setTextColor(Color.parseColor("#FFFFFF"));
			// frameLayout.setBackgroundResource(R.drawable.blank);

		} else {
			textView.setTextColor(Color.parseColor("#FFB600"));
			// frameLayout.setBackgroundResource(R.drawable.blankyellow);
		}
	}

	public CharSequence getText() {
		return textView.getText();
	}

	public String getTag() {
		return (String) textView.getTag();
	}

	public void setTag(String o) {
		textView.setTag(o);
		if (o.equalsIgnoreCase("ON")) {
			textView.setTextColor(Color.parseColor("#000000"));
			// frameLayout.setBackgroundResource(R.drawable.blank);
			// frameLayout.setBackgroundResource(R.drawable.selected_button);
		} else {
			if (textView.getText().toString().equalsIgnoreCase("")) {
				// textView.setTextColor(Color.parseColor("#FFFFFF"));
				// frameLayout.setBackgroundResource(R.drawable.blank);
			} else {
				// textView.setTextColor(Color.parseColor("#FFB600"));
				// frameLayout.setBackgroundResource(R.drawable.blank);
				// frameLayout.setBackgroundResource(R.drawable.create_button);
			}
		}
	}
}
