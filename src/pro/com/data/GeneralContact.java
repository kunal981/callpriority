package pro.com.data;

import java.util.Date;

public class GeneralContact {
	private int id;
	private int contactId;
	private String timeStamp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Date getEndTime() {
		Date endDate;

		long timeStampLong = Long.parseLong(timeStamp);
		long endTimeLong = timeStampLong + (24 * 60 * 60 * 1000);

		endDate = new Date(endTimeLong);

		return endDate;
	}
}
